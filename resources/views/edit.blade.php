<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <title>new-profile</title>
      <link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
      <link href="{{asset('cssw/edit-table.css')}}" rel="stylesheet" type="text/css">
   </head>
   <body>
      <div class="l">
         <nav class="navbar  navbar-expand-lg ">
            <div class="container">
               <a class="navbar-brand logo px-2" href="#">SCOUTERIA</a>
               <button class="navbar-toggler open-menu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"  title="open menu">
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <!---->
      <div class="container-fluid">
      <div class="container mt-5 ">
         <div class="d-flex flex-row ">
            <ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left" role="navigation">
               <li class="nav-item">
                  <a href="#Basic-info" class="nav-link active" data-toggle="tab" role="tab" aria-controls="Basic-Info">Basic-info</a>
               </li>
               <li class="nav-item">
                  <a href="#club" class="nav-link" data-toggle="tab" role="tab" aria-controls="club">club</a>
               </li>
               <li class="nav-item">
                  <a href="#Former-Club" class="nav-link" data-toggle="tab" role="tab" aria-controls="Former-Club">Former-Club</a>
               </li>
               <li class="nav-item">
                  <a href="#Achievements" class="nav-link" data-toggle="tab" role="tab" aria-controls="Achievements">Achievements</a>
               </li>
            </ul>
            <div class="tab-content px-3">
               <div class="tab-pane fade show active" id="Basic-info" role="tabpanel" style=" width:751px !important;">
                  <h1 class="px-5">Basic Info</h1>
                  <div class="container-fluid">
                    <div class="container">
                     <div class="row mt-3">
                        <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                           <p> your Name</p>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                           <span>{{$users->name}}</span>
                        </div>
                        <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                           <img src="images/icons8-edit-24.png" id="ShowRow4">
                        </div>
                     </div>
                     <div class="row mt-3" >
                        <div class="col-12">
                           <form action="{{route('editinfo')}}"  method="post" id="ShowForm4" style="display:none;">
                              @csrf
                              <div class="form-group row">
                                 <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" id="textarea4"/>
                                 </div>
                              </div>
                              <div class="form-group row  "  >
                                 &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                 <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                 &nbsp; &nbsp;&nbsp; 
                                 <button type="button" class="btn btn-outline-success br-40" 
                                    id="closeN">Cancel</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
                     <div class="container">
                        <div class="row mt-3">
                           <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                              <p> your Academy</p>
                           </div>
                           <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                               @if($users->academy) 
                           <span>{{$users->academy}}</span>
                           @else
                           <span>Not Added</span>
                           @endif
                           </div>
                           <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                              <img src="images/icons8-edit-24.png" id="ShowRow5">
                           </div>
                        </div>
                        <div class="row mt-3" >
                           <div class="col-12">
                              <form action="{{route('academy')}}" method="post" id="ShowForm5" style="display:none;">@csrf
                                 <div class="form-group row">
                                    <div class="col-sm-8">
                                       <input type="text" name="academy" class="form-control" id="textarea5"/>
                                    </div>
                                 </div>
                                 <div class="form-group row "  >
                                    &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                    <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                    &nbsp; &nbsp;&nbsp; 
                                    <button type="button" class="btn btn-outline-success br-40" 
                                       id="close22">Cancel</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="container">
                       <div class="row mt-3">
                        <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                           <p> your Weight </p>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                           @if($users->weight) 
                           <span>{{$users->weight}} Kg</span>
                           @else
                           <span>Not Added</span>
                           @endif
                        </div>
                        <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                           <img src="images/icons8-edit-24.png" id="ShowRow22">
                        </div>
                       </div>
                         <div class="row mt-3" >
                           <div class="col-12">
                             <form action="{{route('editWeight')}}"  method="post" id="ShowForm22" style="display:none;">
                                @csrf
                                <div class="form-group row">
                                   <div class="col-sm-8">
                                      <input type="text" name="weight" class="form-control" id="textarea22"/>
                                   </div>
                                </div>
                                <div class="form-group row  "  >
                                   &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                   <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                   &nbsp; &nbsp;&nbsp; 
                                   <button type="button" class="btn btn-outline-success br-40" 
                                      id="closeH">Cancel</button>
                                </div>
                             </form>
                         </div>
                       </div>
                    </div>                     <div class="container">
                       <div class="row mt-3">
                        <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                           <p> your Height </p>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                           @if($users->height) 
                           <span>{{$users->height}} Cm</span>
                           @else
                           <span>Not Added</span>
                           @endif
                        </div>
                        <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                           <img src="images/icons8-edit-24.png" id="ShowRow6">
                        </div>
                       </div>
                         <div class="row mt-3" >
                           <div class="col-12">
                             <form action="{{route('editHeight')}}"  method="post" id="ShowForm6" style="display:none;">
                                @csrf
                                <div class="form-group row">
                                   <div class="col-sm-8">
                                      <input type="text" name="height" class="form-control" id="textarea6"/>
                                   </div>
                                </div>
                                <div class="form-group row  "  >
                                   &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                   <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                   &nbsp; &nbsp;&nbsp; 
                                   <button type="button" class="btn btn-outline-success br-40" 
                                      id="closeH">Cancel</button>
                                </div>
                             </form>
                         </div>
                       </div>
                    </div>
                     <div class="container">
                       <div class="row mt-3">
                          <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                             <p> your Date </p>
                          </div>
                          <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                             @if($users->birthday)
                             <span>{{$users->birthday}}</span>
                             @else
                             <span>Not Added</span>
                             @endif
                          </div>
                          <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                             <img src="images/icons8-edit-24.png" id="ShowRow8">
                          </div>
                       </div>
                       <div class="row mt-3" >
                          <div class="col-12">
                             <form action="{{route('editBirthday')}}"  method="post" id="ShowForm8" style="display:none;">
                                @csrf
                                <div class="form-group row">
                                   <div class="col-sm-8">
                                      <input type="date" name="birthday" class="form-control" id="textarea8"/>
                                   </div>
                                </div>
                                <div class="form-group row  "  >
                                   &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                   <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                   &nbsp; &nbsp;&nbsp; 
                                   <button type="button" class="btn btn-outline-success br-40" 
                                      id="closeD">Cancel</button>
                                </div>
                             </form>
                          </div>
                       </div>
                  </div>
                     <div class="container">
                        <div class="row mt-3">
                           <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                              <p> your City </p>
                           </div>
                           <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                               @if($users->city)
                             <span>{{$users->city}}</span>
                             @else
                             <span>Not Added</span>
                             @endif
                           </div>
                           <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                              <img src="images/icons8-edit-24.png" id="ShowRow9">
                           </div>
                        </div>
                        <div class="row mt-3" >
                           <div class="col-12">
                              <form action="{{route('city')}}" method="post" id="ShowForm9"  style="display:none;">@csrf
                                 <div class="form-group row">
                                    <div class="col-sm-8">
                                       <input type="text" name="city" class="form-control" id="textarea9"/>
                                    </div>
                                 </div>
                                 <div class="form-group row  "  >
                                    &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                    <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                    &nbsp; &nbsp;&nbsp;
                                    <button type="button" class="btn btn-outline-success br-40" 
                                       id="closeCity">Cancel</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                      <div class="container">
                       <div class="row mt-3">
                          <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                             <p> your Country </p>
                          </div>
                          <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                             @if($users->country)
                             <span>{{$users->country->name}}</span>
                             @else
                             <span>Not Added</span>
                             @endif
                          </div>
                          <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                             <img src="images/icons8-edit-24.png" id="ShowRow10">
                          </div>
                       </div>
                       <div class="row mt-3" >
                          <div class="col-12">
                             <form action="{{route('editCountry')}}" method="post" id="ShowForm10"  style="display:none;">
                                @csrf
                                <div class="form-group row">
                                   <div class="col-sm-8">
                                      <select  name="country" class="form-control custom-select" onchange=" java_script_:show2(this.options[this.selectedIndex].value)" name="country">
                                         @foreach($countries as $country)
                                         <option value="{{$country->id}}">{{$country->name}}</option>
                                         @endforeach
                                      </select>
                                   </div>
                                </div>
                                <div class="form-group row  "  >
                                   &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                   <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                   &nbsp; &nbsp;&nbsp;
                                   <button type="button" class="btn btn-outline-success br-40" 
                                      id="closeCountry">Cancel</button>
                                </div>
                             </form>
                          </div>
                       </div>
                  </div>
                    <div class="container">
                     <div class="row mt-3">
                        <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                           <p> your Sport </p>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                           @if($users->sport) 
                           <span>{{$users->sport->sport}}</span>
                           @else
                           <span>Not Added</span>
                           @endif
                        </div>
                        <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                           <img src="images/icons8-edit-24.png" id="ShowRow11">
                        </div>
                     </div>
                     <div class="row mt-3" >
                        <div class="col-12">
                           <form action="{{route('editSport')}}" method="post" id="ShowForm11"  style="display:none;">
                              @csrf
                              <div class="form-group row">
                                 <div class="col-sm-8">
                                    <select class="form-control custom-select" onchange=" java_script_:show2(this.options[this.selectedIndex].value)" name="sport" >
                                       @foreach($sports as $sport)
                                       <option value="{{$sport->id}}">{{$sport->sport}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group row  "  >
                                 &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                 <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                 &nbsp; &nbsp;&nbsp;
                                 <button type="button" class="btn btn-outline-success br-40" 
                                    id="closeSport">Cancel</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
                  </div>
               </div>
                <div class="tab-pane fade" id="club" role="tabpanel">
                   <h1 class="px-5">club</h1>
                   <div class="container-fluid">
                      <div class="container">
                         <div class="row mt-3">
                            <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                               <p> your Club</p>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                               <span>Test</span>
                            </div>
                            <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                               <img src="images/icons8-edit-24.png" id="ShowRow3">
                            </div>
                         </div>
                         <div class="row mt-3" >
                            <div class="col-12">
                               <form action="{{route('saveclub')}}" method="post" id="ShowForm3" style="display:none;">
                                  @csrf
                                  <div class="form-group row">
                                     <select class="form-control custom-select" onchange=" java_script_:show2(this.options[this.selectedIndex].value)" name="club" >
                                        @foreach($clubs as $club)
                                        <option value="{{$club->id}}">{{$club->name}}</option>
                                        @endforeach
                                     </select>
                                  </div>
                                  <div class="form-group row  justify-content-center"  >
                                     <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                     &nbsp; &nbsp; 
                                     <button type="button" class="btn btn-outline-success br-40" 
                                        id="closeC">Cancel</button>
                                  </div>
                               </form>
                            </div>
                         </div>
                      </div>
                  </div>
                </div>
               <div class="tab-pane fade" id="Former-Club" role="tabpanel">
                  <h2 class="px-5">Former Club</h2>
                  <div class="container-fluid">
                     <div class="container">
                        <div class="row mt-3">
                           <div  class="col-sm-7 col-md-7 col-lg-7 col-xl-7 ">
                              <p> your Former Club</p>
                           </div>
                           <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                              <span>Test</span>
                           </div>
                           <div  class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                              <img src="images/icons8-edit-24.png" id="ShowRow2">
                           </div>
                        </div>
                        <div class="row mt-3" >
                           <div class="col-12">
                              <form action="" method="post" id="ShowForm2" style="display:none;">
                                 <div class="form-group row">
                                    <div class="col-sm-8">
                                       <input type="text" class="form-control" id="textarea2"/>
                                    </div>
                                 </div>
                                 <div class="form-group row  "  >
                                    &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                    <button type="button" class="btn btn-outline-success br-40 ">Save </button>
                                    &nbsp; &nbsp; 
                                    <button type="button" class="btn btn-outline-success br-40" 
                                       id="closeF">Cancel</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
                <div class="tab-pane fade" id="Achievements" role="tabpanel">
               <h2>Achievements</h2>
               <div class="container-fluid">
                  <div class="container">
                     <div class="row mt-3">
                        <div  class="col-sm-12 col-md-12 col-lg-7 col-xl-7 ">
                           <p> Achievements</p>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 ">
                           <span>Test</span>
                        </div>
                        <div  class="col-sm-12 col-md-12 col-lg-2 col-xl-2">
                           <img src="images/icons8-edit-24.png" id="ShowRow">
                        </div>
                     </div>
                     <div class="row mt-3" >
                        <div class="col-12">
                           <form action="{{route('achievments')}}" method="post" id="ShowForm" style="display:none;">
                              @csrf
                              <div class="form-group row">
                                 <div class="col-sm-8">
                                    <textarea  name="achievments" class="form-control" id="textarea"></textarea>
                                 </div>
                              </div>
                              <div class="form-group row "  >
                                 &nbsp; &nbsp;  
                                 <button type="submit" class="btn btn-outline-success br-40 ">Save </button>
                                 &nbsp; &nbsp;  
                                 <button type="button" class="btn btn-outline-success br-40" 
                                    id="closeA">Cancel</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </div>
         </div>
      </div>
      <script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
      <script src="{{asset('jsw/popper.min.js')}}"></script>
      <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('jsw/edit.js')}}"></script>
   </body>
</html>