                  <li class="nav-item dropdown">
                     <a  href="#" title="NOTIFICATIONS" id="button" 
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-bell home"></i> 
                        <span class="nav-counter2 ">{{$users->unreadNotifications()->where('data->type','confirmed','rejected')->get()->count()}}</span>
                        <div class="dropdown-menu popover fade show bs-popover-top " 
                           role="tooltip" id="item" x-placement="bottom"  style="display:none;">
                           <ul class=" px-1 ">
                              <li style="text-align:center;" >
                                 <p class="msg">NOTIFICATIONS</p>
                              </li>
                           </ul>
                           <div class="EaRLIER2 mb-3 ">
                              <p class="msg">EARLIER</p>
                           </div>
                           <div class="popover-body">
                              <ul class="unstyled px-1 mt-1">
                                 @foreach($users->unreadNotifications()->where('data->type','confirmed','rejected')->get() as $notification)
                                 <li data-alert_id="1" class="alert_li mb-2">
                     <a href="{{route('profileuser',$notification->data['user'])}}">
                     <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="ImgWhoFollow" ></a>
                     <a href="{{route('profileuser',$notification->data['user'])}}" class="alert_message">
                     {{$notification->data['message']}}
                     </a>
                     <br>
                     <a href=="{{route('profileuser',$notification->data['user'])}}" class="time_alert">
                     {{$notification->created_at}}
                     </a>
                     <div class="clearfix"></div><br>
                     </li> @endforeach 
                     <br>
                     <div class="noti-footer mb-5">Read more</div>
                     </ul></div></div>
                     </a>  
                  </li>
                  &nbsp; &nbsp;
                  <li class="nav-item dropdown">
                     <a  href="#" title="Followers" id="button2"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-user home"></i>
                        <span class="nav-counter2 ">{{$users->unreadNotifications()->where('data->type','send_request')->get()->count()}}</span>
                        <div class="dropdown-menu popover fade show bs-popover-top " 
                           role="tooltip" id="item2" x-placement="bottom"  style="display:none;">
                           <ul class=" px-1 py-1">
                              <li style="text-align:center;" class="msg">Follow Requests</li>
                           </ul>
                           <div class="EaRLIER ">
                              <p>EARLIER</p>
                           </div>
                           <div class="popover-body">
                              <ul class="unstyled px-1">
                                 @foreach($users->unreadNotifications()->where('data->type','send_request')->get() as $notification)
                                 <li data-alert_id="1" class=" py-2">
                                    <div class="alert_li2">
                        <a href="{{route('profileuser',$notification->data['user'])}}" >
                        <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="ImgWhoFollow">        </a>
                        <a href="{{route('profileuser',$notification->data['user'])}}" class="request alert_li2">{{$notification->data['message']}}</span>
                        </a>
                        </div>
                        <div class="container">
                        <div class="row justify-content-end">
                        <form action="{{route('ConfirmFollow')}}"> 
                        <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
                        <input type="hidden" name="id" value="{{$notification->id}}">  
                        <button type="submit" title="Accept" class="Accept"><a href=""> 
                        <i class="far fa-check-circle ">
                        </i>
                        </a></button>
                        </form>
                        &nbsp;&nbsp;
                        <form action="{{route('rejectFollow')}}">
                        <input type="hidden" name="user_id" value="{{$notification->data['user']}}">
                        <input type="hidden" name="id" value="{{$notification->id}}">            
                        <button type="submit" title="REject" class="Accept"><a href=""> 
                        <i class="far fa-times-circle"></i>
                        </a></button>
                        </form>
                        </div> 
                        </div>
                        <div class="clearfix"></div>
                        </li> @endforeach
                        <br>
                        <div class="noti-footer mb-5 mm">Read more</div>
                        </ul>
                        </div></div>
                        </a>     
                  </li>


                  <!-- NOT HERE -->
                  <div class="tab-pane fade" id="pendding-just" role="tabpanel" aria-labelledby="pendding-tab-just">
                     <div class="container first ">
                        <div class="row">
                           @foreach($users->unreadNotifications()->where('data->type','send_request')->get() as $notification)
                           <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                              <div class="card ">
                                 <div class="box">
                                    <div class="img">
                                       <img  src="{{asset('uploads')}}/{{$notification->data['image']}}">
                                    </div>
                                    <h2>{{$notification->data['name']}}</h2>
                                    <p>  {{$notification->data['message']}}</p>
                                    <div class="container ">
                                       <div class="row ">
                                          <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                                             <form action="{{route('ConfirmFollow')}}">
                                                <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
                                                <input type="hidden" name="id" value="{{$notification->id}}"> 
                                                <button  class="btn btn-outline-success  btn-custom1" type="submit">
                                                Accept</button>
                                             </form>
                                          </div>
                                          <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                             <form action="{{route('rejectFollow')}}">
                                                <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
                                                <button  class="btn btn-outline-success  btn-custom2  " type="submit">
                                                Reject  
                                                </button>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                           <br>
                           <div class="line mt-3"></div>
                           <br>    
                        </div>
                     </div>
                  </div>