 <!doctype html>
<html>
<head>
<meta charset="utf-8">
    
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>forget password</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
  <link href="{{asset('cssw/forget.css')}}" rel="stylesheet" type="text/css">
    
</head>
<body >
    
  <nav class="navbar  navbar-expand-lg color-nav">
    <div class="container">
      
      <a class="navbar-brand logo py-2" href="#">SCOUTERIA</a>


      <div class="collapse navbar-collapse" id="navbarSupportedContent">


          
      </div>
        </div>
  </nav>
    
<div class="container " style="margin-top:4%;">
  <div class="row justify-content-center">
      <h3 class="reset px-3">Reset Your Password</h3>
    </div>
    
    <div class="row justify-content-center">
 <p class="how px-3">How do you want to receive the message to reset your password?</p>
     </div>
    
    
    
<div class="row justify-content-center">


 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 " >
    <div class="container">
        <div class="row justify-content-center">
  <h2   class="sec-code  mt-5">Enter Your Email</h2> 
        </div>
        <div class="row justify-content-center">
              <div class="code mb-5 ">
    <div class="container">
        <div class="row justify-content-center py-3 ">
             <p class="please-check">Please check your email for a text message with your link to reset password.</p>
         </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
  <form method="POST" action="{{ route('password.email') }}">
    @csrf
          <div class="row justify-content-center Enter-Code">
          <input id="email" type="email" class=" br-40  enter-code " placeholder="Enter Your Email" name="email" value="{{ old('email') }}"  required>

          @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
         </div>
        <div class="row justify-content-center py-2">
       <button type="submit" class="btn btn-success btn-custom">Continue</button>
        </div>

  </form>    
           </div> 
            </div>
        </div>
        

             </div>
     
        </div>  <!------end of main col------> 
    </div> <!------end of main row------> 
       </div><!------end of main container------> 
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

<script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('jsw/popper.min.js')}}"></script>
  <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('jsw/forget.js')}}"></script>
</body>
</html>
