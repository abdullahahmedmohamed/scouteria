 <!doctype html>
<html>
<head>
<meta charset="utf-8">
    
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>forget password</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
<link href="{{asset('cssw/renew.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
  <div class="full">
<div class="l">
    <nav class="navbar  navbar-expand-lg ">
    <div class="container">
      <a class="navbar-brand logo px-2" href="#">SCOUTERIA</a>
      <button class="navbar-toggler open-menu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"  title="open menu">
      </button>
        

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
    
      </div>
        </div>
  </nav>
  </div>
    
<div class="container " style="margin-top:4%;">
  <div class="row justify-content-center">
      <h3 class="reset px-3">Reset Your Password</h3>
    </div>
    
    <div class="row justify-content-center">
 <p class="how px-3">How do you want to receive the code to reset your password?</p>
     </div>
    
    

        
        <div class="row justify-content-center ">
        <h2   class="sec-code  ">Choose a new password</h2>
        </div>
        <div class="row justify-content-center ">
         <div class="code">
     <div class="container">
         <div class="row justify-content-center py-3 ">
             <p class="please-check">Please check your phone for a text message with your code. Your code is 6 characters in length</p>
         </div>
        <form method="POST" action="{{ route('password.reset') }}">
                        @csrf

            <input type="hidden" name="token" value="{{ $token }}">
         <div class="row justify-content-center">
              <input id="email" type="email" class="br-40 "  placeholder="Enter Email" name="email" value="{{ $email ?? old('email') }}" required autofocus>

              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
         </div>            
         <div class="row justify-content-center">
             <span class="new ">Enter new Password&nbsp;: </span>&nbsp;&nbsp;&nbsp;&nbsp; <input class=" br-40  New-password " name="password" type="password" placeholder="Enter password" aria-required=”true”    required>

              @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
           </div>

         <div class="row  justify-content-center py-3">
             <span class="new "> Confirm  Password&nbsp;: </span>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; <input class=" br-40  New-password " id="password-confirm"
              name="password_confirmation" type="password"  placeholder="confirm password" aria-required=”true”    required></div>
         
            <div class="row justify-content-center ">
       <button type="supmit" class="btn btn-success btn-custom">Continue</button>
        </div>
      </form>
      </div>
         </div>
        </div>
        </div>
             </div>
     
        </div>  <!------end of main col------> 
    </div> <!------end of main row------> 
       </div><!------end of main container------> 
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

<script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('jsw/popper.min.js')}}"></script>
  <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('jsw/forget.js')}}"></script>
</body>
</html>
