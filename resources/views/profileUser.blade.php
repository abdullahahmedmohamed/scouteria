<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <title>profile friend</title>
      <link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
      <link href="{{asset('cssw/profile_end.css')}}" rel="stylesheet" type="text/css">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
   </head> 
<body>
     <nav class="navbar navbar-expand-lg nav-color " >
         &nbsp; &nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <div class="container">
            <a class="navbar-brand logo " href="#"> &nbsp;SCOUTERIA</a>
            <button class="navbar-toggler togg" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" title="open-menu">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto ">
                  <li class="nav-item  search ">
                     <span class="fa fa-search search-icon px-3 py-2"></span>
                     <input class="form-control br-40   px-5 " type="search" placeholder="Search" aria-label="Search">
                  </li>
                  &nbsp; &nbsp;
                  <li class="nav-item ">
                     <a class="nav-link " href="{{route('profile')}}" title="profile">
                     <img src="{{asset('uploads')}}/{{$users->image}}" class="ImgWhoFollow ">    
                     <span class="name" title="profile">{{$users->name}}</span></a>                 
                  </li>
                  &nbsp; &nbsp;
                 
                  &nbsp; &nbsp;
                  <li class="nav-item dropdown">
                     <a  href="#" title="NOTIFICATIONS" id="button" 
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-bell home"></i> 
                        <span class="nav-counter2 ">{{count($notifications_news)}}</span>
                        <div class="dropdown-menu popover fade show bs-popover-top " 
                           role="tooltip" id="item" x-placement="bottom"  style="display:none;">
                           <ul class=" px-1 ">
                              <li style="text-align:center;" >
                                 <p class="msg">NOTIFICATIONS</p>
                              </li>
                           </ul>
                           <div class="EaRLIER2 mb-3 ">
                              <p class="msg">EARLIER</p>
                           </div>
                           <div class="popover-body">
                              <ul class="unstyled px-1 mt-1">
                                
                                 @foreach($notifications_news as $notification)
                                 <li data-alert_id="1" class="alert_li mb-2">
                     <a href="{{route('profileuser',$notification->data['user'])}}">
                     <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="ImgWhoFollow" ></a>
                     <a href="{{route('profileuser',$notification->data['user'])}}" class="alert_message">
                     {{$notification->data['message']}}
                     </a>
                     <br>
                     <a href=="{{route('profileuser',$notification->data['user'])}}" class="time_alert">
                     {{$notification->created_at->diffForHumans()}}
                     </a>
                     <div class="clearfix"></div><br>
                     </li> @endforeach 
                     
                     <br>
                     <div class="noti-footer mb-5">Read more</div>
                     </ul></div></div>
                     </a>  
                  </li>
                  &nbsp; &nbsp;
                  <li class="nav-item dropdown">
                     <a  href="#" title="Followers" id="button2"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-user home"></i>
                        <span class="nav-counter2 ">{{count($notifications_request)}}</span>
                        <div class="dropdown-menu popover fade show bs-popover-top " 
                           role="tooltip" id="item2" x-placement="bottom"  style="display:none;">
                           <ul class=" px-1 py-1">
                              <li style="text-align:center;" class="msg">Follow Requests</li>
                           </ul>
                           <div class="EaRLIER ">
                              <p>EARLIER</p>
                           </div>
                           <div class="popover-body">
                              <ul class="unstyled px-1">
                                  
                                 @foreach($notifications_request as $notification)
                                 
                                 <li data-alert_id="1" class=" py-2">
                                    <div class="alert_li2">
                     <a href="{{route('profileuser',$notification->data['user'])}}" >
                     <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="ImgWhoFollow">        </a>
                     <a href="{{route('profileuser',$notification->data['user'])}}" class="request alert_li2">{{$notification->data['message']}}</span>
                     </a>
                     </div>
                     <div class="container">
                     <div class="row justify-content-end">
                     <form action="{{route('ConfirmFollow')}}"> 
                     <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
                     <input type="hidden" name="id" value="{{$notification->id}}">  
                     <button type="submit" title="Accept" class="Accept"><a href=""> 
                     <i class="far fa-check-circle ">
                     </i>
                     </a></button>
                     </form>
                     &nbsp;&nbsp;
                     <form action="{{route('rejectFollow')}}">
                     <input type="hidden" name="user_id" value="{{$notification->data['user']}}">
                     <input type="hidden" name="id" value="{{$notification->id}}">            
                     <button type="submit" title="REject" class="Accept"><a href=""> 
                     <i class="far fa-times-circle"></i>
                     </a></button>
                     </form>
                     </div> 
                     </div>
                     <div class="clearfix"></div>
                     </li> @endforeach
                    
                     <br>
                     <div class="noti-footer mb-5 mm">Read more</div>
                     </ul>
                     </div></div>
                     </a>     
                  </li>

                  &nbsp; &nbsp; 
                  <li class="nav-item  dropdown">
                     <a  href="{{route('logout')}}" title="Settings" >
                     <i class="fa fa-fw fa-cog home"></i>
                     </a> 
                  </li>
               </ul>
            </div>
         </div>
      </nav>
  
      <div id="all">
    
      <br>
      <!---->       
      <div class="container-fluid ">
         <div class="container">
            <div class="row ">
               <div class="col-sm-1 col-md-1 col-lg-1  ">    
               </div>
               <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 ">
                  <div class="container">
                     <div class="row ">
                        <img src="{{asset('uploads')}}/{{$freinds->image}}" class=" img-profile img-fluid" >
                     </div>
                     <div class="row py-2">
                        <p class="  player-name " >{{$freinds->name}} &nbsp;&nbsp; &nbsp; &nbsp;   </p>
                        <a href="#" id="OpenModalForEdit">
                        </a>
                     </div>
                  </div>
               </div>
               <div class="col-sm-12 col-md-10 col-lg-8 col-xl-7">
                  <div  id="table-div" >
                     <table class="table ">
                        <tbody>
                           <tr > 
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;
                              
                           </tr>
                           <tr >
                              <th >  Name</th>
                              <td>{{$freinds->name}}</td>
                              <th>Age</th>
                              <td>23</td>
                           </tr>
                           <tr>
                              <th>Height</th>
                              <td>{{$freinds->height}} cm</td>
                              <th>Weight</th>
                              <td>{{$freinds->weight}} kg</td>
                           </tr>
                           <tr>
                              <th>Sport</th>
                              @if($freinds->sport)
                              <td>{{$freinds->sport->sport}}</td>
                              @else
                              Not Added
                              @endif
                              <th>Club</th>
                              <td>
                                 @if($freinds->club)
                                 {{$freinds->club->name}}
                                 @else
                                 Not Added
                                 @endif
                              </td>
                           </tr>
                           <tr>
                              <th>Former club</th>
                              <td>cccccc</td>
                              <th>City</th>
                              <td>Cairo</td>
                           </tr>
                           <tr>
                              <th>Date of birth</th>
                              <td>{{$freinds->birthday}}</td>
                              <th>Type </th>
                              <td>{{$freinds->type->type}}</td>
                           </tr>
                           <tr>
                              <th>Academy</th>
                              <td>cccccc</td>
                              <th>Country</th>
                              @if($freinds->country)
                              <td>{{$freinds->country->name}}</td>
                              @else
                              Not Added
                              @endif
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
           
            <div class="row tabs-all py-3">
               <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">   
               </div>
               <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 ">
                  <div class="btn-group ">
                     &nbsp;&nbsp;&nbsp;
                     <form action="{{route('follow_user')}}" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$freinds->id}}">
                        <button type="submit" class="btn btn-follow">FOLLOW</button>
                     </form>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <form action="" method="">
                        <button type="button" class="btn btn-follow">Messages</button>
                     </form>
                  </div>
               </div>
               <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1 ">
                  <a href="#" id="Achievements" class="nav-links">
                     <h4 >&nbsp;Achievements</h4>
                  </a>
               </div>
               <div class="col-sm-12 col-md-2 col-lg-1 col-xl-1">   
               </div>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1 ">
                  <a href="#" id="videos"  class="nav-links ">
                     <h4 >Videos</h4>
                  </a>
               </div>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
               <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1 ">
                  <a href="#" id="followers"  class="nav-links">
                     <h4 >Followers</h4>
                  </a>
               </div>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;  
               <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1 ">
                  <a href="#" id="Posts"  class="nav-links">
                     <h4 >Posts</h4>
                  </a>
               </div>
            </div>
            <!--row for show links hidden div-->       
            <div class="row" id="tt">
               <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">   </div>
               <div class="col-sm-10 col-md-10 col-lg-12 col-xl-10 ">
                  <div id="Achievements1" style="display:none;" class="justifu-content-center" >
                     <h5 id="b">
                        @if($freinds->achievments)
                        {{$freinds->achievments}}
                        @else
                        <h5 id="b"> Not Added </h5>
                        @endif
                     </h5>
                  </div>
               </div>
               <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" >
                  <div id="Videos1" style="display:none" >
                     <div class="container-fluid">
                        <div class="container ">
                           <div class="row ">
                              @foreach($freinds->posts()->orderBy('created_at','DESC')->get() as $post)
                              @foreach($post->postmedia as $media) 
                              @switch($media->media_type_id )
                              @case('1')
                              <span></span>
                              @break  
                              @case('2') 
                              <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">   
                              </div>
                              <div class="col-sm-4 col-md-6 col-lg-4 col-xl-3">
                                 <div class="container">
                                    <div class="row ">
                                       <a href=""> 
                                       <img src="{{asset('uploads')}}/{{$freinds->image}}" class="ImgWhoFollow">
                                       </a>
                                       <a href=""  class="owner_video px-1">
                                       {{$freinds->name}}
                                       <br>
                                       <span class="time_date">{{$post->created_at->diffForHumans()}}</span>
                                       </a>       
                                    </div>
                                    <div class="row ">
                                       <div class="col-sm-6 col-md-6 col-xl-6 col-lg-6" >
                                          <a data-toggle="modal" data-target="#basicExampleModal">
                                             <video width="200px" height="100%" >
                                                <source src="{{url('imgPosts/'.$media->media_path )}}">
                                             </video>
                                          </a>
                                          <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                             <div class="modal-dialog" role="document" id="modal">
                                                <div class="modal-content" >
                                                   <div class="modal-header">
                                                      <h5 class="modal-title" id="exampleModalLabel">First Video</h5>
                                                      <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                      </button>
                                                   </div>
                                                   <div class="modal-body">
                                                      <video width="100%" height="100%" controls>
                                                         <source src="{{url('imgPosts/'.$media->media_path )}}">
                                                      </video>
                                                   </div>
                                                   <div class="modal-footer">
                                                      <div class="footer-comments">
                                                         <div class="container">
                                                            <div class="row">
                                                               <div class="col-md-12 ">
                                                                  <div class="panel ">
                                                                     <div class="panel-heading head">
                                                                        <ul class="nav panel-tabs py-2">
                                                                           <li class="views ">
                                                                           </li>
                                                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                           <li class=" views  " id="show-who-like">
                                                                              <a href="" data-toggle="tab">
                                                                              <i class="fas fa-heart "></i>
                                                                              {{$post->like->count()}}  likes</a>
                                                                           </li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                           <li class="views " id="show-who-comment">
                                                                              <a href="#" data-toggle="tab">
                                                                              <i class="fas fa-comment"></i>
                                                                              {{$post->comment->count()}} comments</a>
                                                                           </li>
                                                                           
                                                                           <li class=" views  " title="share">
                                                                             
                                                                           </li>
                                                                           &nbsp;&nbsp;&nbsp;
                                                                        </ul>
                                                                     </div>
                                                                     <div class="panel-body">
                                                                        <div class="tab-content">
                                                                           <div class="tab-pane " id="tab1">
                                                                              <div class="container" >
                                                                                 <div class="row justify-content-center mb-2">
                                                                                    <h6 class="title">People Who like This Video</h6>
                                                                                 </div>
                                                                                 <div class="row">
                                                                                    <a href="" > 
                                                                                    <img src="images/scott_caldwell_4.png" class="ImgWhoFollow">
                                                                                    </a>
                                                                                    <a href="" class="views1 px-2 ">
                                                                                    <span >Hecham ahmed</span> 
                                                                                    </a>
                                                                                    <a href="" class=" mutual">
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                                    <span class=" mutual">2 mutual Scouts</span> 
                                                                                    </a>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <div class="row px-5">
                                                                                       <i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;
                                                                                       <span class="time_date">9 jun 2019 &nbsp;3:40 am</span>
                                                                                    </div>
                                                                                 </div>
                                                                                 <br>
                                                                              </div>
                                                                           </div>
                                                                           <div class="tab-pane" id="tab2">
                                                                              <div class="container">
                                                                                 <div class="row justify-content-center mb-2">
                                                                                    <h6 class="title">People Who Comment This Video</h6>
                                                                                 </div>
                                                                                 @foreach($post->comment as $comments)
                                                                                 <div class="row">
                                                                                    <a href="" > 
                                                                                    <img src="{{asset('uploads')}}/{{$comments->user->image}}" class="ImgWhoFollow">
                                                                                    </a>
                                                                                    <a href="" class="views1 px-2 ">
                                                                                    <span >{{$comments->user->name}}</span> 
                                                                                    </a>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <span class=" px-5 comment-video">&nbsp;&nbsp; {{$comments->text}}</span>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;
                                                                                    @if($comments->user_id == $id)
                                                                                    <div class="row px-5 " >
                                                                                       <br>
                                                                                       <form action="" method="">
                                                                                          <button class="icons" id="Edit"> 
                                                                                          <i class="fas fa-pencil-alt" title="Edit Your Comment" ></i>
                                                                                          <input type="hidden" value="{{$comments->id}}">
                                                                                          </button>
                                                                                          &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                           <a class="icons" href="{{route('removeComment',$comments->id)}}" class="icons" onclick="document.getElementByclassName('MyForm').submit(); return false;"> 
                                                                                          <i class="fas fa-trash-alt" title="Delete Your Comment" ></i></a>
                                                                                          
                                                                                          &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        
                                                                                       </form>
                                                                                    </div>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <div class="row px-5 " >
                                                                                       <form action="{{route('editComment',$comments->id)}}" id="textareav" style="display:none">
                                                                                          <div class="form-group row">
                                                                                             <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                                                                                <img src="images/scott_caldwell_4%20(2).png" class="px-2" >
                                                                                             </div>
                                                                                             <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">
                                                                                                 <input type="hidden" value="{{$comments->id}}">
                                                                                                <input type="text" name="comment" class="form-control comment"  placeholder="Edit Comment" id="open1v"/>
                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="form-group row justify-content-center " id="Openv1" style="display:none;">
                                                                                             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                             <button type="submit" class="btn btn-outline-success  btn-custom">comment</button>
                                                                                             &nbsp; &nbsp; &nbsp;
                                                                                             <button type="button"  class="btn btn-outline-success   btn-custom" id="cancelv1">Cancel</button>
                                                                                          </div>
                                                                                       </form>
                                                                                       <form action="" method="post" id="textareav2" style="display:none">
                                                                                          <div class="form-group row">
                                                                                             <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                                                                                <img src="images/scott_caldwell_4%20(2).png" class="px-2">
                                                                                             </div>
                                                                                             <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">
                                                                                                <input type="text" class="form-control comment"  placeholder="Write New Comment" id="openv1"/>
                                                                                             </div>
                                                                                          </div>
                                                                                          <div class="form-group row justify-content-center " id="OPenv1" style="display:none;">
                                                                                             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                             <button type="button" class="btn btn-outline-success  btn-custom">comment</button>
                                                                                             &nbsp; &nbsp; &nbsp;
                                                                                             <button type="button"  class="btn btn-outline-success  btn-custom" id="cancelv1">Cancel</button>
                                                                                          </div>
                                                                                       </form>
                                                                                    </div>
                                                                                     
                                                                                       @else
                                                                                       <span></span>
                                                                                       @endif
                                                                                 </div>
                                                                                  
                                                                                 <br>@endforeach 
                                                                                 <br>
                                                                                 <div class="row">
                                                                                    <form action="{{route('writeComment')}}">
                                                                                       <div class="form-group row">
                                                                                          <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                                                                             <img src="images/scott_caldwell_4%20(2).png" class="px-2">
                                                                                          </div>
                                                                                          <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">
                                                                                             <input type="text" name="comment" class="form-control comment"  placeholder="Write a Comment" id="open1"/>
                                                                                             <input type="hidden" name="post_user" value="{{$post->user_id}}">
                                                                                             <input type="hidden" name="post_id" value="{{$post->id}}">
                                                                                             <input type="hidden" name="user_id" value="{{$users->id}}">
                                                                                             <input type="hidden" name="name" value="{{$users->name}}">
                                                                                             <input type="hidden" name="image" value="{{$users->image}}">
                                                                                          </div>
                                                                                       </div>
                                                                                       <div class="form-group row justify-content-center " id="Open1" style="display:none;">
                                                                                          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                          <button type="submit" class="btn btn-outline-success  btn-custom">comment</button>
                                                                                          &nbsp; &nbsp; &nbsp;
                                                                                          <button type="button"  class="btn btn-outline-success   btn-custom" id="cancel1">Cancel</button>
                                                                                       </div>
                                                                                    </form>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row ">
                                       <div class="col-sm-6 col-md-11 col-lg-10 col-xl-11">
                                          {{$post->text}}       .
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @break
                              @default
                              @endswitch
                              @endforeach 
                              @endforeach
                           </div>
                         
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <div id="Followers2" style="display:none">
                     <div class="container mt-3">
                        <ul class="nav nav-tabs nav-justified md-tabs" id="myTabJust" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" id="Followers-tab-just" data-toggle="tab" href="#Followers-just" role="tab" aria-controls="Followers-just" aria-selected="true">Followers</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" id="Following-tab-just" data-toggle="tab" href="#Following-just" role="tab" aria-controls="Following-just" aria-selected="false">Following</a>
                           </li>
                           
                        </ul>
                        <div class="tab-content card2 px-4 py-4" id="myTabContentJust">
                           <div class="tab-pane fade show active" id="Followers-just" role="tabpanel" aria-labelledby="Followers-tab-just">
                              <div class="container first ">
                                 <div class="row">
                                    @if(count($followers)) 
                                    @foreach($followers as $follower)
                                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                       <div class="card ">
                                          <div class="box">
                                             <a href="#">
                                                <div class="img">
                                                   <img src="{{asset('uploads')}}/{{$follower->image}}">
                                                </div>
                                             </a>
                                             <a href="{{route('profileuser',$follower->id)}}">
                                                <h2>{{$follower->name}} </h2>
                                             </a>
                                             <p> Wants To Follow You.</p>
                                             <div class="container ">
                                                <div class="row justify-content-center">
                                                   <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                      <form action="{{route('unFollow')}}" method="post">
                                                         @csrf
                                                         <input type="hidden" name="user" value="{{$follower->id}}"> 
                                                         <button  class="btn btn-outline-success  btn-custom" type="submit">Unfollow</button>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    @endforeach
                                    @endif 
                                    <br>
                                    <div class="line mt-3"></div>
                                 </div>
                              </div>
                           </div>
                           <!--this is second link following-->
                           <div class="tab-pane fade" id="Following-just" role="tabpanel" aria-labelledby="Following-tab-just">
                              <div class="container first ">
                                 <div class="row">
                                    @if(count($following))
                                    @foreach($following as $follow)
                                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                       <div class="card ">
                                          <div class="box">
                                             <a href="#">
                                                <div class="img">
                                                   <img src="{{asset('uploads')}}/{{$follow->image}}">
                                                </div>
                                             </a>
                                             <a href="{{route('profileuser',$follow->id)}}">
                                                <h2>{{$follow->name}}</h2>
                                             </a>
                                             <p> Wants To Follow You.</p>
                                             <div class="container ">
                                                <div class="row justify-content-center">
                                                   <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                      <form action="{{route('unFollow')}}" method="post">
                                                         @csrf
                                                         <input type="hidden" name="user" value="{{$follow->id}}"> 
                                                         <button  class="btn btn-outline-success  btn-custom" type="submit">Unfollow</button>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    @endforeach
                                    @endif    
                                    <br>
                                    <div class="line mt-3"></div>
                                 </div>
                              </div>
                           </div>
                           <!--this is second link pendding-->

                        </div>
                     </div>
                  </div>
                  <!--end of container-->   
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                     <div class="container " id="Posts1">
                        <div class="row ">
                           <div class="col-sm-12 col-md-12 col-lg-6 col-xl-8 ">
                              <div class="container">
                                 <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">  </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-9 sec ">

                                    </div>
                                 </div>
                              </div>
                              <br>  @foreach($freinds->posts()->orderBy('created_at','DESC')->get() as $post)
                              <div class="container">
                                 <div class="row ">
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">  
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-9 sec ">
                                       <a href="#" class="">  
                                       <img class="rounded-circle post-img2 "  src="{{asset('uploads')}}/{{$freinds->image}}" />
                                       <span class="post-owner py-3 ">{{$freinds->name}}</span>
                                       <span class="shared-a-post ">shared a post.</span>
                                       </a>
                                       <div class="dropdown ">
                                          <i class="  burger-menu" ></i>  
                                          <div class="dropdown-content">
                                             <form action="#" method="post" id="my_form">
                                     
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row  ">
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">  
                                    </div>
                                    <div class="col-sm-10 col-md-12 col-lg-12 col-xl-9 sec">
                                       <p class="text px-2" > 
                                          &nbsp; {{$post->text}}
                                       </p>
                                    </div>
                                 </div>
                                 @foreach($post->postmedia as $media) 
                                 @switch($media->media_type_id )
                                 @case('1')
                                 <div class="row ">
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">  
                                    </div>
                                    <div class="col-sm-10 col-md-12 col-lg-12 col-xl-9 sec">
                                       <div class="style-ImgPost py-1">
                                          <img src="{{url('imgPosts/'.$media->media_path )}}" class="ImgPost"> 
                                       </div>
                                    </div>
                                 </div>
                                 @break
                                 @case('2')
                                 <div class="row ">
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">  
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-9 sec">
                                       <div class="style-VideoPost py-2">
                                          <video class="VideoPost" controls>
                                             <source src="{{url('imgPosts/'.$media->media_path )}}"  type="video/mp4">
                                          </video>
                                       </div>
                                    </div>
                                 </div>
                                 @break
                                 @default
                                 @endswitch
                                 @endforeach
                                 <div class="row ">
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">   
                                    </div>
                                    <div class="col-sm-10 col-md-12 col-lg-12 col-xl-9 sec">
                                       <div class="count-comment_like">
                                          <div class="container">
                                             <div class="row justify-content-around">
                                                <form action="{{route('like')}}">
                                                   <input type="hidden" name="post_id" value="{{$post->id}}"> 
                                                   <input type="hidden" name="post_user" value="{{$post->user_id}}">
                                                   <input type="hidden" name="name" value="{{$users->name}}">
                                                   <input type="hidden" name="image" value="{{$users->image}}">    
                                                   <a class="icons2" id="ShowLikesPosts">
                                                   <span class="count_like " >{{$post->like->count()}} Likes</a><button type="submit" class="icons2" ><i class="fas fa-thumbs-up">
                                                   </i>
                                                   </span>
                                                   </button>
                                                </form>
                                                <button class="icons2" id="ShowCommentsPosts">
                                                <span class="count_like ">{{$post->comment->count()}} Comments <i class="fas fa-comment">
                                                </i>
                                                </span>
                                                </button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row " > 
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">  
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9 sec">
                                       <div id="ShowPeopleLikesPosts" style="display:none" class="mt-2"> 
                                         @foreach($post->like as $likes) 
                                          <a href="" > 
                                          <img src="{{asset('uploads')}}/{{$likes->user->image}}" class="ImgWhoFollow" class="mb-2">
                                          </a>
                                          <a href="" class="views1 px-2 ">
                                          <span >{{$likes->user->name}}</span> 
                                          </a>
                                          <a href="" class=" mutual">
                                          <i class="fas fa-thumbs-up"></i>  
                                          </a>
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                          <br> @endforeach
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 ">  
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9 ">
                                       <div id="ShowPeopleCommentsPosts" style="display:none" class="mt-2">
                                          @foreach($post->comment as $comments)

                                          &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                          <a href="" > 
                                          <img src="{{asset('uploads')}}/{{$comments->user->image}}" class="ImgWhoFollow">
                                          </a>
                                          <a href="" class="views1 px-2 ">
                                          <span >{{$comments->user->name}}</span> 
                                          </a>
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          <span class=" px-4 comment-video">{{$comments->text}}</span>
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                          
                                          <div class="row justify-content-center " >
                                             <a href="#" class="icons"> @if($comments->user_id == $id)
                                             <i class="fas fa-pencil-alt" title="Edit Your Comment" id="EditPost"></i>
                                             </a>
                                             &nbsp;&nbsp;&nbsp;&nbsp;
                                             
                                             
                                            
                                             &nbsp;&nbsp;&nbsp;
                                             <a href="{{route('removeComment',$comments->id)}}" class="icons" onclick="document.getElementByclassName('MyForm').submit(); return false;"> 
                                             <i class="fas fa-trash-alt" title="Delete Your Comment" id="DeleteYourComment"></i>
                                             </a>  @endif  
                                          </div>
                                          <div class="row  justify-content-center mt-2" >
                                             <form action="{{route('editComment',$comments->id)}}"  id="textareaP1" style="display:none" class="MyForm">
                                                <div class="form-group row">
                                                   <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                                      <img src="{{asset('uploads')}}/{{$users->image}}"  class="px-2 ImgWhoFollow" >
                                                   </div>
                                                   <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">
                                                      <input type="hidden" value="{{$comments->id}}">
                                                      <input type="text"  name="comment" class="form-control comment"  placeholder="Edit New Comment" id="editP1"/>
                                                   </div>
                                                </div>
                                                <div class="form-group row justify-content-center " id="EditP1" style="display:none;">
                                                   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                   <button type="submit" class="btn btn-outline-success btn-custom">comment</button>
                                                   &nbsp; &nbsp; &nbsp;
                                                   <button type="button"  class="btn btn-outline-success btn-custom" id="cancelEditP1">Cancel</button>
                                                </div>
                                             </form>
                                          
                                          </div> @endforeach
                                          <div class="row  justify-content-center mt-2" >
                                             <form action="{{route('writeComment')}}" id="textareaP1NewComment" >
                                                <div class="form-group row">
                                                   <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                                      <img src="{{asset('uploads')}}/{{$users->image}}"  class="px-2 ImgWhoFollow">
                                                   </div>
                                                   <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">
                                                      <input type="text" name="comment" class="form-control comment"  placeholder="Write New Comment" id="writeNewComment"/>
                                                      <input type="hidden" name="post_user" value="{{$post->user_id}}">
                                                      <input type="hidden" name="post_id" value="{{$post->id}}">
                                                      <input type="hidden" name="user_id" value="{{$users->id}}">
                                                      <input type="hidden" name="name" value="{{$users->name}}">
                                                      <input type="hidden" name="image" value="{{$users->image}}">
                                                   </div>
                                                </div>
                                                <div class="form-group row justify-content-center "  name="comment"  id="WriteNewComment" style="display:none;">
                                                   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                   <button type="submit"" class="btn btn-outline-success  btn-custom">comment</button>
                                                   &nbsp; &nbsp; &nbsp;
                                                   <button type="button"  class="btn btn-outline-success  btn-custom" id="cancelEditP1NewComment">Cancel</button>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach

                              <br>
                           </div>
                           <div class="col-sm-12 col-md-12  col-lg-6 col-xl-3 ">
                              <div class="container sec-div">
                                 <div class="row  justify-content-between">
                                    <strong class="Who_to_follow">
                                       <p class="Who_to_follow  mt-3">
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          Who to follow
                                          <small > 
                                          <a  href="" class="view-all ">&nbsp;&nbsp;View all</a>
                                          </small>
                                          &nbsp;
                                          <small > 
                                          <a  href="" class="Refresh ">Refresh</a>
                                          </small>
                                       </p>
                                    </strong>
                                 </div>

                                    @if(count($all))

                                    @foreach($all as $alll)
                                    @if($users->id == $alll->id)
                                    <span></span>
                                    @else
                                 <div class="row justify-content-center mb-2">   
                                    <a href="{{route('profileuser',$alll->id)}}"  > 
                                    <img src="{{asset('uploads')}}/{{$alll->image}}" class="ImgWhoFollow">
                                    </a>
                                    <a href="{{route('profileuser',$alll->id)}}" class="post-owner ">&nbsp;{{$alll->name}}</a>  
                                    &nbsp; <span class="" ></span>
                                 </div>
                                   @endif
                                   @endforeach
                                   @endif
                              
                              </div>
                              <br>
               
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-1 ">   
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
      <script src="{{asset('jsw/popper.min.js')}}"></script>
      <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('jsw/profile_end.js')}}"></script>
   </body>
</html>

