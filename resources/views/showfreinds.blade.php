<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>new-profile</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
<link href="{{asset('cssw/edit-table.css')}}" rel="stylesheet" type="text/css">

</head>
<body>

<div class="l">
    <nav class="navbar  navbar-expand-lg ">
    <div class="container">
      <a class="navbar-brand logo px-2" href="#">SCOUTERIA</a>
      <button class="navbar-toggler open-menu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"  title="open menu">
      </button>
        

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
    
      </div>
        </div>
  </nav>
	</div>
   <!---->


<div class="container py-3" >
<div class="row">
<div class="col-sm-4 col-md-3 col-lg-3 col-xl-3">
  
   @if(count($user))

          @foreach($user as $users)

<form action="{{route('follow_user')}}" method="POST">
  @csrf
<img src="{{asset('uploads')}}/{{$users->image}}" class=" img-profile px-1" >
  <input type="hidden" name="user_id" value="{{$users->id}}">
              <button type="submit" href="" class="btn btn-info"> <i class="fa fa-home"></i>  follow </button><br>

</form><br>
<a href="{{route('profileuser',$users->id)}}"><h3> {{$users->name}} </h3></a>
        @endforeach
   @endif

</div>
</div>
</div>























<script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('jsw/popper.min.js')}}"></script>
<script src="{{asset('jsw/bootstrap.min.js')}}"></script>

</body>
</html>
