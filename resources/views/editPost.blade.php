<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>EditPost</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
<link href="{{asset('cssw/profile_end.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cssw/editpost.css')}}" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
	
     <nav class="navbar navbar-expand-lg nav-color " >
         &nbsp; &nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <div class="container">
            <a class="navbar-brand logo " href="#"> &nbsp;SCOUTERIA</a>
            <button class="navbar-toggler togg" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" title="open-menu">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto ">
                  <li class="nav-item  search ">
                     <span class="fa fa-search search-icon px-3 py-2"></span>
                     <input class="form-control br-40   px-5 " type="search" placeholder="Search" aria-label="Search">
                  </li>
                  &nbsp; &nbsp;
                  <li class="nav-item ">
                     <a class="nav-link " href="{{route('profile')}}" title="profile">
                     <img src="{{asset('uploads')}}/{{$users->image}}" class="ImgWhoFollow ">    
                     <span class="name" title="profile">{{$users->name}}</span></a>                 
                  </li>
                  &nbsp; &nbsp;
                 
                  &nbsp; &nbsp;
                  <li class="nav-item dropdown">
                     <a  href="#" title="NOTIFICATIONS" id="button" 
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-bell home"></i> 
                        <span class="nav-counter2 ">{{count($notifications_news)}}</span>
                        <div class="dropdown-menu popover fade show bs-popover-top " 
                           role="tooltip" id="item" x-placement="bottom"  style="display:none;">
                           <ul class=" px-1 ">
                              <li style="text-align:center;" >
                                 <p class="msg">NOTIFICATIONS</p>
                              </li>
                           </ul>
                           <div class="EaRLIER2 mb-3 ">
                              <p class="msg">EARLIER</p>
                           </div>
                           <div class="popover-body">
                              <ul class="unstyled px-1 mt-1">
                                
                                 @foreach($notifications_news as $notification)
                                 <li data-alert_id="1" class="alert_li mb-2">
                     <a href="{{route('profileuser',$notification->data['user'])}}">
                     <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="ImgWhoFollow" ></a>
                     <a href="{{route('profileuser',$notification->data['user'])}}" class="alert_message">
                     {{$notification->data['message']}}
                     </a>
                     <br>
                     <a href=="{{route('profileuser',$notification->data['user'])}}" class="time_alert">
                     {{$notification->created_at->diffForHumans()}}
                     </a>
                     <div class="clearfix"></div><br>
                     </li> @endforeach 
                     
                     <br>
                     <div class="noti-footer mb-5">Read more</div>
                     </ul></div></div>
                     </a>  
                  </li>
                  &nbsp; &nbsp;
                  <li class="nav-item dropdown">
                     <a  href="#" title="Followers" id="button2"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-user home"></i>
                        <span class="nav-counter2 ">{{count($notifications_request)}}</span>
                        <div class="dropdown-menu popover fade show bs-popover-top " 
                           role="tooltip" id="item2" x-placement="bottom"  style="display:none;">
                           <ul class=" px-1 py-1">
                              <li style="text-align:center;" class="msg">Follow Requests</li>
                           </ul>
                           <div class="EaRLIER ">
                              <p>EARLIER</p>
                           </div>
                           <div class="popover-body">
                              <ul class="unstyled px-1">
                                  
                                 @foreach($notifications_request as $notification)
                                 
                                 <li data-alert_id="1" class=" py-2">
                                    <div class="alert_li2">
                     <a href="{{route('profileuser',$notification->data['user'])}}" >
                     <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="ImgWhoFollow">        </a>
                     <a href="{{route('profileuser',$notification->data['user'])}}" class="request alert_li2">{{$notification->data['message']}}</span>
                     </a>
                     </div>
                     <div class="container">
                     <div class="row justify-content-end">
                     <form action="{{route('ConfirmFollow')}}"> 
                     <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
                     <input type="hidden" name="id" value="{{$notification->id}}">  
                     <button type="submit" title="Accept" class="Accept"><a href=""> 
                     <i class="far fa-check-circle ">
                     </i>
                     </a></button>
                     </form>
                     &nbsp;&nbsp;
                     <form action="{{route('rejectFollow')}}">
                     <input type="hidden" name="user_id" value="{{$notification->data['user']}}">
                     <input type="hidden" name="id" value="{{$notification->id}}">            
                     <button type="submit" title="REject" class="Accept"><a href=""> 
                     <i class="far fa-times-circle"></i>
                     </a></button>
                     </form>
                     </div> 
                     </div>
                     <div class="clearfix"></div>
                     </li> @endforeach
                    
                     <br>
                     <div class="noti-footer mb-5 mm">Read more</div>
                     </ul>
                     </div></div>
                     </a>     
                  </li>

                  &nbsp; &nbsp; 
                  <li class="nav-item  dropdown">
                     <a  href="{{route('logout')}}" title="Settings" >
                     <i class="fa fa-fw fa-cog home"></i>
                     </a> 
                  </li>
               </ul>
            </div>
         </div>
      </nav>
         
  <!---->       
    


<div class="container-fluid">
<div class="container">
	<!--row for showing textarea-->	
	<div class="row">
<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
		</div>
<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">	
<form  action="{{route('changePost',$post->id)}}" class="FormForEditPost" method="POST" enctype="multipart/form-data"> 
                     	@csrf
<div class="container headrrow3">	
	<div class="row  justify-content-start mb-3">	
<div class="headerrow ">	
<h3>&nbsp;&nbsp;Edit Post</h3>	
	
</div>
</div>
<div class="row justify-content-start mb-2">
	<div class="headerrow2">
	<a href="#" class="post-row1">  
      &nbsp;
		<img class="rounded-circle post-img2 "  src="{{asset('uploads')}}/{{$users->image}}" />
         </a>
	<textarea name="body">{{$post->text}}</textarea>

		
		
		
	</div>
	</div>
<div class="row justify-content-start mb-3 ">	
<div class="headerrow px-3">
<label class="btn-upload mt-2">
	 <input type="hidden" name="idimg" value="">   
   <input type="file" name="image" >
   <i class="fas fa-images post-icons"></i>&nbsp; Upload Photo
</label>

		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

	</div>
	</div>
<div class="row justify-content-start">	
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn2 ReturnUserToProfile">  Cancel</button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;
	<button type="submit" class="btn3 ReturnUserToNewPostInProfile">  Share</button>

	
	</div>
	</form>
		</div>
	<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
		</div>	
	</div>
	
	
	
	
	
	</div>
	</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
  <script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('jsw/editPost.js')}}"></script>
  <script type="text/javascript" src="{{asset('jsw/profile_end.js')}}"></script>
</body>
</html>
