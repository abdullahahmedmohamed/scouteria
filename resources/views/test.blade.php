<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>profile</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
<link href="{{asset('cssw/profile_end.css')}}" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <div id="all">
<nav class="navbar navbar-expand-lg nav-color mb-4" >
<div class="container px-2">
  <a class="navbar-brand logo  offset-1" href="#">SCOUTERIA</a>
  <button class="navbar-toggler togg" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" title="open-menu">
    <span class="navbar-toggler-icon"></span>
  </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto ">
                 

      <li class="nav-item  search ">
          <span class="fa fa-search search-icon px-3 py-2">
          </span>
            <input class="form-control br-40   px-5 " type="search" placeholder="Search" aria-label="Search">
        </li> 
         &nbsp; &nbsp;
               <li class="nav-item ">
                  <a class="nav-link " href="#" title="profile">
                   <img src='#' class="img ">    
                     <span class="name" title="profile">{{$users->name}}</span></a>                 
         </li>  
        
          &nbsp; &nbsp;&nbsp;
                <li class="nav-item  ">
                 <a  href="#" title="Home">
                   <i class="fa fa-fw fa-home home"></i> 
                     </a> 
         </li>
           &nbsp; &nbsp;&nbsp;
               <li class="nav-item  dropdown">
                 <a  href="#" title="Message" id="button3">
                   <i class="fa fa-fw fa-envelope home"><sup class="count">10</sup></i> 
                  <div class="dropdown-menu popover fade show bs-popover-top " 
           role="tooltip" id="item3" x-placement="bottom"  style="display:none;">
             <ul class=" px-3 mb-2">
                 <li>
                     <a href="" class="msg">Recent(13)
                     </a>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="" class="msg">Messages Request
                     </a>
                 </li>
                      </ul>
          <div class="EaRLIER"><p>EARLIER</p></div>
                 <div class="popover-body">
            <ul class="unstyled px-1">
    <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

    <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
    <p  style="text-align:left">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix"></div>
      </li><br>
                        <div class="noti-footer mb-5 py-4">Read more</div>

           <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

   <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

            <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="{{asset('uploads')}}/{{$users->image}}" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

  <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

</ul></div></div>   
    
                     
                             
                     
                     
                     
                     </a> 
         </li>  
         &nbsp; &nbsp;&nbsp;
        
        <li class="nav-item dropdown">  
       <a  href="#" title="NOTIFICATIONS" id="button" 
          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  
         <i class="fa fa-fw fa-bell home"><sup class="count2">{{$users->unreadNotifications()->where('data->type','confirmed','rejected')->get()->count()}}</sup></i> 
           </i>

<!--this is div for notification -->

      <div class="dropdown-menu popover fade show bs-popover-top " 
           role="tooltip" id="item" x-placement="bottom"  style="display:none;">
             <ul class=" px-1 ">
                 <li style="text-align:center;" ><p class="msg">NOTIFICATIONS</p></li>
             </ul>

             <div class="EaRLIER2 mb-3 "><p class="msg">EARLIER</p></div>
                 <div class="popover-body">
                    <ul class="unstyled px-1 mt-1">
                        @foreach($users->unreadNotifications()->where('data->type','confirmed','rejected')->get() as $notification)
                         <li data-alert_id="1" class="alert_li mb-2">
                            <a href="{{route('profileuser',$notification->data['user'])}}">
                            <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="notification-img  " ></a>
                            <a href="{{route('profileuser',$notification->data['user'])}}" class="alert_message">
                                {{$notification->data['message']}}
                             </a>
                             <br>
                              <a href="#" class="time_alert">
                                {{$notification->created_at}}
                              </a>
                            <div class="clearfix"></div><br>
                         </li>
                            @endforeach 

                         <br>

                         <div class="noti-footer mb-5">Read more</div>
                    </ul>
                 </div>
      </div>



        </a>  </li>
    
         &nbsp; &nbsp;&nbsp;
        <li class="nav-item dropdown">  
       <a  href="#" title="Followers" id="button2"
       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  
        <i class="fa fa-fw fa-user home"><sup class="count">{{$users->unreadNotifications()->where('data->type','send_request')->get()->count()}}</sup>
           </i>
      <div class="dropdown-menu popover fade show bs-popover-top " 
           role="tooltip" id="item2" x-placement="bottom"  style="display:none;">
             <ul class=" px-1 py-1">
               <li style="text-align:center;" class="msg">Follow Requests</li>
          </ul>
          <div class="EaRLIER "><p>EARLIER</p></div>
                 <div class="popover-body">
  <ul class="unstyled px-1">

    @foreach($users->unreadNotifications()->where('data->type','send_request')->get() as $notification)

       <li data-alert_id="1" class=" py-2">
         <div class="alert_li2">
                <a href="{{route('profileuser',$notification->data['user'])}}" >
                  <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="notification-img  " >
                </a>
                <a href="#" class="request alert_li2"> 
                  <span>{{$notification->data['message']}}</span>
                </a>
          </div>
           <div class="container">
        <div class="row justify-content-end">

             
        <form action="{{route('ConfirmFollow')}}">
        <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
        <input type="hidden" name="id" value="{{$notification->id}}"> 
              <button type="submit" title="Accept" class="Accept"><a href=""> 
                              <i class="far fa-check-circle "> </i>
                                </a>
                        </button>
                   </form>
                    &nbsp;&nbsp;
             <form action="{{route('rejectFollow')}}">
                 <input type="hidden" name="user_id" value="{{$notification->data['user']}}">
                 <input type="hidden" name="id" value="{{$notification->id}}">

            <button type="submit" title="Reject"><i class="far fa-times-circle"></i></button>
            </form>
        </div> 
            </div>

 
               <div class="clearfix"></div>
                   @endforeach    
                    <br>
               <div class="noti-footer mb-5 mm">Read more</div>
          </ul>
        </div>
        </div>

           
                  
            </a>     

    
      &nbsp; &nbsp; 
            
        <li class="nav-item  dropdown">
                 <a  href="{{route('logout')}}" title="Settings" >
              <i class="fa fa-fw fa-cog home"></i>
                     </a> 
         </li>  
        </ul>
    </div>  
            </div>
</nav>       
  <!---->       
    
<div class="container-fluid ">    
 <div class="container">    
  <div class="row "> 
          <div class="col-sm-1 col-md-1 col-lg-1 col-xl-0 ">    
      </div>
    <div class="col-sm-2 col-md-10 col-lg-3 col-xl-3 ">    
<img src="{{asset('uploads')}}/{{$users->image}}" class=" img-profile" >
<h4  class=" py-2  player-name" >{{$users->name}}</h4>
      </div>
                
          <div class="col-sm-12 col-md-10 col-lg-8 col-xl-7">    
<div  id="table-div" >
<table class="table ">
    <tbody>
      <tr >     
        <th >  Name</th>
        <td>{{$users->name}}</td>
          <th>Age</th>
        <td>23</td>
      </tr>
        
        <tr>
        <th>Height</th>
        <td>{{$users->height}} cm</td>
        <th>Weight</th>
        <td>{{$users->weight}} kg</td>
      </tr>
        
       <tr>
        <th>Sport</th>
        @if($users->sport)
        <td>{{$users->sport->sport}}</td>
        @else
          Not Added
        @endif
        <th>Club</th>
        <td>
         @if($users->club)
              {{$users->club->name}}
          @else
          Not Added

          @endif</td>
      </tr>
        
      <tr>
        <th>Former club</th>
        <td>cccccc</td>
     <th>City</th>
        <td>Cairo</td>
      </tr>
        
       <tr>
        <th>Date of birth</th>
        <td>{{$users->birthday}}</td>
        <th>Type </th>
        <td>{{$users->type->type}}</td>
      </tr>
        
      <tr>
        <th>Academy</th>
        <td>cccccc</td>
        <th>Country</th>
         @if($users->sport)
        <td>{{$users->country->name}}</td>
        @else
          Not Added
        @endif
      </tr>

    </tbody>
  </table>
      </div>
     </div>

 </div>
 <!---->       
<div class="row tabs-all">     
      <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">   
    </div>
  <div class="col-sm-12 col-md-3 col-lg-3 col-xl-1">   
        <div class="btn-group">
                <form action="{{route('follow_user')}}" method="POST">
                  @csrf

                  <input type="hidden" name="user_id" value="{{$users->id}}">
                  <button type="submit" class="btn btn-block  btn-custom ">FOLLOW</button>
               </form>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <form action="" method="">
    <button type="button" class="btn btn-block  btn-custom ">Messages</button>
            </form>

        </div>
    </div>
    
          
     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;
   <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 ">   
          <a href="#" id="Achievements" class="nav-links">
               <h4 >Achievements</h4>
           </a>
     
          

        </div>
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
       <div class="col-sm-12 col-md-2 col-lg-2 col-xl-1 ">   
          <a href="#" id="videos"  class="nav-links ">
              <h4 >Videos</h4>
          </a>

        </div>
     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
      <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 ">   
             <a href="#" id="followers"  class="nav-links">
                <h4 >Followers</h4>
             </a>
          

        </div>
   <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 ">   
             <a href="#" id="Posts"  class="nav-links">
                <h4 >Posts</h4>
             </a>
          

        </div>

     </div> 
  
      <!--row for show links hidden div-->       

    <div class="row" id="tt">

          <div class="col-sm-12 col-md-12 col-lg-2 col-xl-2 ">   
        </div>
      <div class="col-sm-12 col-md-10 col-lg-8 col-xl-8 ">   
     <div id="Achievements1" style="display:none;" class="justifu-content-center" >
<h5 id="b">      

  Messi was awarded FIFA World Player of the Year in 2009,
       His playing style and skills are very similar to the Argentine legend ,Diego Maradona. There is much competition between him and Portuguese footballer Cristiano Ronaldo because of their similar 
      Messi began football at a young age and his potential was quickly seen by Barcelona
         </h5>     
             </div>
        </div>
       <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" >   
     <div id="Videos1" style="display:none" >
<div class="container-fluid">
 <div class="container ">
        <div class="row ">
                                               @foreach($users->posts()->orderBy('created_at','DESC')->get() as $post)
                                     @foreach($post->postmedia as $media) 
                              @switch($media->media_type_id )
                                                                   @case('1')
                              <span></span>
          
                  @break  
                  @case('2') 
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
            <div class="col-sm-12 col-md-4 col-lg-5 col-xl-3">  
  
          <div class="container">
        <div class="row ">

    <a href=""> 
          <img src="images/scott_caldwell_4.png">
    </a>
      
    <a href=""  class="owner_video px-1">
        {{$users->name}}
        <br>
        <span class="time_date">{{$post->created_at}}</span>
    </a>       
                </div>       
        
            <div class="row ">            
                        


           
  <div class="col-sm-6 col-md-6 col-xl-6 col-lg-6" > 
   <a data-toggle="modal" data-target="#basicExampleModal">
                <video  >
                      <source src="{{url('imgPosts/'.$media->media_path )}}">
          </video>
      </a>
        
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" id="modal">
    <div class="modal-content" >
      <div class="modal-header">


        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <video width="100%" height="100%" controls>
                      <source src="{{url('imgPosts/'.$media->media_path )}}">
          </video>


              

        
      </div>
      <div class="modal-footer">
<div class="footer-comments">
  <div class="container">
  <div class="row">
    <div class="col-md-12 ">
            <div class="panel ">
                <div class="panel-heading head">
                        <ul class="nav panel-tabs py-2">
                      <li class="views "><i class="fas fa-eye"></i>
                         
                          300 views</li>
                                                   
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <li class=" views  " id="show-who-like">
                                <a href="" data-toggle="tab">
                                    <i class="fas fa-heart "></i>
                                    {{$post->like->count()}} likes</a></li>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           
                            <li class="views " id="show-who-comment">
                                <a href="#" data-toggle="tab">
                                    <i class="fas fa-comment"></i>
                                    28 comments</a>
                            </li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <li class=" views  " title="share">
                                <a href="#">
                                    <i class="fas fa-share"></i>
                                </a></li>
                      &nbsp;&nbsp;&nbsp;
                        </ul>
                </div>
                
                <div class="panel-body">
                    <div class="tab-content">
                        
                        
      <div class="tab-pane " id="tab1">
       <div class="container" > 
           <div class="row justify-content-center mb-2">
               <h6 class="title">People Who like This Video</h6>
           </div>
         
          <div class="row">
      <a href="" > 
          <img src="images/scott_caldwell_4.png">
              </a>
              <a href="" class="views1 px-2 ">
                  <span >Hecham ahmed</span> 
              </a>
              <a href="" class=" mutual">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                  <span class=" mutual">2 mutual Scouts</span> 
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="row px-5">
              <i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;
                      <span class="time_date">9 jun 2019 &nbsp;3:40 am</span>
              </div>
           </div>     
                      


    


           <br>
          </div>
             </div>
<div class="tab-pane" id="tab2">
           <div class="container"> 
           <div class="row justify-content-center mb-2">
               <h6 class="title">People Who Comment This Video</h6>
           </div>
         

@foreach($post->comment as $comments)
          <div class="row">
      <a href="" > 
          <img src="images/scott_caldwell_4.png">
              </a>
              <a href="" class="views1 px-2 ">
                  <span >{{$comments->name}}</span> 
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span class=" px-5 comment-video">{{$comments->text}} 
</span>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
              <div class="row px-5 " >
            <a href="#" class="icons"> 
                <i class="fas fa-pencil-alt" title="Edit Your Comment" id="Edit"></i>
                  </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" class="icons"> 
                 <i class="fas fa-trash-alt" title="Delete Your Comment" ></i>
                  </a>
                 &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" class="icons"> 
                      <i class="fas fa-thumbs-down" title="Dislike Your Comment"></i>
                  </a>
                  &nbsp;&nbsp;&nbsp;
            <a href="#" class="icons"> 
                      <i class="fas fa-reply-all" title="reply" id="Reply"></i>
                  </a>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="row px-5 " >
               <form action="" method="post" id="textarea" style="display:none">
                        <div class="form-group row">
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                              <img src="images/scott_caldwell_4%20(2).png" class="px-2">
                            </div>
                       <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">

                           <input type="text" class="form-control comment"  placeholder="Write New Comment" id="open1"/></div>
                        </div>
                            <div class="form-group row justify-content-center " id="Open1" style="display:none;">
                                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                         <button type="button" class="btn btn-outline-success btn1">comment</button>
                                                   &nbsp; &nbsp; &nbsp;
                       <button type="button"  class="btn btn-outline-success  btn1" id="cancel1">Cancel</button>
 
                               
                         </div>
                  </form>
                  <form action="" method="post" id="textarea2" style="display:none">
                        <div class="form-group row">
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                              <img src="images/scott_caldwell_4%20(2).png" class="px-2">
                            </div>
                       <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">

                           <input type="text" class="form-control comment"  placeholder="Write New Comment" id="open1"/></div>
                        </div>
                              <div class="form-group row justify-content-center " id="Open1" style="display:none;">
                                   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                           <button type="button" class="btn btn-outline-success btn1">comment</button>
                                                     &nbsp; &nbsp; &nbsp;
                         <button type="button"  class="btn btn-outline-success  btn1" id="cancel1">Cancel</button>
 
                               
                         </div>
                </form>
              </div>
           </div>
 
         
                      <br>



               <br>
               <div class="row">
                <form action="" method="post">
                        <div class="form-group row">
                            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                              <img src="images/scott_caldwell_4%20(2).png" class="px-2">
                            </div>
                       <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">

                           <input type="text" class="form-control comment"  placeholder="Write a Comment" id="open1"/></div>
                        </div>
        <div class="form-group row justify-content-center " id="Open1" style="display:none;">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
     <button type="button" class="btn btn-outline-success btn1">comment</button>
                               &nbsp; &nbsp; &nbsp;
   <button type="button"  class="btn btn-outline-success  btn1" id="cancel1">Cancel</button>
 
                               
                         </div>
                         </form>
                  
               </div>           
          </div>        @endforeach  

                        </div>
                        
                       
                    </div>
                </div>
            </div>
        </div>
  </div>
</div>
          </div>
        </div>
    </div>
  </div>
</div>
</div>
      </div> 
          <div class="row ">
             <div class="col-sm-6 col-md-11 col-lg-10 col-xl-11">
                 Messi progressed through the ranks and was given his.
                 </div>
                   </div>
                    </div> 
                     </div>          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;

                    
    </div>
        @break
                 @default
                     @endswitch
        @endforeach 
     @endforeach      
   </div>
        </div>
        </div>
     </div>
       </div> 
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">   
     
     <div id="Followers2" style="display:none">
    <div class="container mt-3">

    
  <ul class="nav nav-tabs nav-justified md-tabs" id="myTabJust" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="Followers-tab-just" data-toggle="tab" href="#Followers-just" role="tab" aria-controls="Followers-just" aria-selected="true">Followers</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="Following-tab-just" data-toggle="tab" href="#Following-just" role="tab" aria-controls="Following-just" aria-selected="false">Following</a>
  </li>
            
            <li class="nav-item">
    <a class="nav-link" id="pendding-tab-just" data-toggle="tab" href="#pendding-just" role="tab" aria-controls="pendding-just" aria-selected="false">Pendding</a>
  </li>
        
             
</ul>
<div class="tab-content card2 px-4 py-4" id="myTabContentJust">
  <div class="tab-pane fade show active" id="Followers-just" role="tabpanel" aria-labelledby="Followers-tab-just">
    
<div class="container first ">
 <div class="row">
          @if(count($followers)) 
          @foreach($followers as $follower)
  <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
       <div class="card ">
          <div class="box">
            <a href="#">
              <div class="img">
                  <img src="{{asset('uploads')}}/{{$follower->image}}">

                </div></a>
<a href="{{route('profileuser',$follower->id)}}"><h2>{{$follower->name}} </h2></a> 
              <p> Wants To Follow You.</p>
           <div class="container ">
             <div class="row justify-content-center">
                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                        <form action="{{route('unFollow')}}" method="post">
                            @csrf
                  <input type="hidden" name="user" value="{{$follower->id}}"> 

                           <button  class="btn btn-outline-success  btn-custom"type="submit">Unfollow</button>
                                  
                          </form>
                 </div> 

               </div>  
            </div>

          </div>
        </div>
          
    </div>           @endforeach
           @endif 
            <br>
        <div class="line mt-3"></div>

 </div>

 </div>


</div>
  
    
    
    
    
    
    
    
    
    
          <!--this is second link following-->
    
    
<div class="tab-pane fade" id="Following-just" role="tabpanel" aria-labelledby="Following-tab-just">
 <div class="container first ">
   <div class="row">
        @if(count($following))
          @foreach($following as $follow)
    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
      <div class="card ">
        <div class="box">
          <a href="#">
            <div class="img">
                <img src="{{asset('uploads')}}/{{$follow->image}}">

             </div></a>
           <a href="{{route('profileuser',$follow->id)}}"> <h2>{{$follow->name}}</h2> </a>
            <p> Wants To Follow You.</p>
            <div class="container ">
             <div class="row justify-content-center">
              <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                <form action="{{route('unFollow')}}" method="post">
                    @csrf
                  <input type="hidden" name="user" value="{{$follow->id}}"> 
                  <button  class="btn btn-outline-success  btn-custom" type="submit">Unfollow</button>
                         
                </form>
              </div> 

             </div>  
            </div>
         </div>
      </div>
    
    </div> 
          @endforeach
        @endif    
    <br>
    <div class="line mt-3"></div>
    
    </div>
    </div>
  


  </div>
  
    
     
    
    
    
    
    
    
    
    
    
    
          <!--this is second link pendding-->
    
    
    
    
    <div class="tab-pane fade" id="pendding-just" role="tabpanel" aria-labelledby="pendding-tab-just">
    <div class="container first ">
<div class="row">

@foreach($users->unreadNotifications()->where('data->type','send_request')->get() as $notification)
<div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
    <div class="card ">
    <div class="box">
        <div class="img">
            <img  src="{{asset('uploads')}}/{{$notification->data['image']}}">

          </div>
        <h2>{{$notification->data['name']}}</h2>
        <p>  {{$notification->data['message']}}</p>
      <div class="container ">
        <div class="row ">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
              <form action="{{route('ConfirmFollow')}}">
        <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
        <input type="hidden" name="id" value="{{$notification->id}}"> 


             <button  class="btn btn-outline-success  btn-custom1" type="submit">
              Accept</button>
                        
             </form>
           </div> 
          <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
              <form action="{{route('rejectFollow')}}">
                 <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 

                   <button  class="btn btn-outline-success  btn-custom2  " type="submit">
                    Reject  
                  </button>
                   
              </form> 
          </div> 
       </div>  
      </div>

    </div>
</div>
    
    </div> 
    
   @endforeach
<br>
                <div class="line mt-3"></div><br>    </div>
   </div>
          
    
    
    
         
                
                
        </div>
       </div>
    
    
    
    </div>

















    
 </div><!--end of container-->   


  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 ">   
    <div id="Posts1" style="display:none">
       <div class="container ">
                        @foreach($users->posts()->orderBy('created_at','DESC')->get() as $post)

        <div class="row ">

         <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8 ">   
          <div class="container ">
            <div class="row ">   
                &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-1 ">   
                    <a href="#">  
                           <img class="rounded-circle " width="90" src="{{asset('uploads')}}/{{$users->image}}"
                    </a>
                  </div>
                   <div class="col-sm-10 col-md-10 col-lg-6 col-xl-8 ">   
                      <div class="mt-3">   
                      &nbsp; &nbsp;&nbsp;&nbsp; 
        
                       <a href="#" class="post-owner py-3 ">{{$users->name}}</a>  
                        <span class="shared-a-post ">shared a post.</span>
                              &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
                         <div class="dropdown">
                            <i class="fas fa-ellipsis-h  burger-menu" ></i>  
                             <div class="dropdown-content">
                                 <form method=""><br>
                                    <a href="#"><i class="fas fa-pencil-alt" ></i>&nbsp;Edit Post</a><br>
                                    <a href="#"><i class="fas fa-trash"></i>&nbsp;Delete Post</a><br>
                                   </form>
                             </div>
                         </div>
                      </div>
                        <br>
                          <div class="" style="float:left; margin-top:-30px;"> 
                                  &nbsp;&nbsp; &nbsp;&nbsp;  <span class="post-time ">{{$post->created_at}}&nbsp;&nbsp;  <i class="fas fa-globe-asia"></i></span>  
                          </div>
                 
                    </div>


             </div>
                <div class="row"> 
                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                       <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 ">   
                          <p class="text"> 
                            {{$post->text}}
                                </p>    
                       </div> 
                </div> 
                  <div class="row ">   
                   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   
                     <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 ">   
                        <div class="count-comment_like"> 
                          <div class="container">
                           <div class="row justify-content-around">
                             <a href="#"  id="ShowLikesPosts">
                                <span class="count_like">3 Likes <i class="fas fa-thumbs-up">
                                    </i>
                               </span>
                             </a>
                              <a href="#" id="ShowCommentsPosts">
                                <span class="count_like ">4 Comments <i class="fas fa-comment">
                               </i>
                               </span>
                              </a>
                            </div>
                          </div>
                        </div> 
                      </div>
                  </div>
                  <div class="row" >
                   <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 ">   
                      <div id="ShowPeopleLikesPosts" style="display:none" class="mt-2"> 
                             &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="" > 
                              <img src="images/scott_caldwell_4.png" class="mb-2">
                            </a>
                            <a href="" class="views1 px-2 ">
                                <span >Hecham ahmed</span> 
                            </a>
                            <a href="" class=" mutual">
                               <i class="fas fa-thumbs-up"></i>  
                             </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;

                           <a href="" class=" mutual">
                                <span class=" mutual">2 mutual Scouts</span> 
                            </a>
                           <br>



                      </div>
                   </div>
                  </div>
                  <div class="row" >
                    <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 ">   
                     <div id="ShowPeopleCommentsPosts" style="display:none" class="mt-2">  
                          &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                            <a href="" > 
                            <img src="images/scott_caldwell_4.png">
                                </a>
                                <a href="" class="views1 px-2 ">
                                    <span >Hecham ahmed</span> 
                                </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class=" px-4 comment-video">this is new video but i don't like it..</span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 
                                <div class="row justify-content-center " >
                                      <a href="#" class="icons"> 
                                          <i class="fas fa-pencil-alt" title="Edit Your Comment" id="EditPost"></i>
                                            </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                      <a href="#" class="icons"> 
                                           <i class="fas fa-trash-alt" title="Delete Your Comment" ></i>
                                            </a>
                                           &nbsp;&nbsp;&nbsp;&nbsp;
                                      <a href="#" class="icons"> 
                                                <i class="fas fa-thumbs-down" title="Dislike Your Comment"></i>
                                            </a>
                                            &nbsp;&nbsp;&nbsp;
                                      <a href="#" class="icons"> 
                                                <i class="fas fa-reply-all" title="reply" id="ReplyPost"></i>
                                            </a>
                                </div>
                                
                          <div class="row  justify-content-center mt-2" >
                             <form action="" method="post" id="textareaP1" style="display:none">
                                          <div class="form-group row">
                                              <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                                <img src="images/scott_caldwell_4%20(2).png" class="px-2">
                                              </div>
                                         <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">

                                             <input type="text" class="form-control comment"  placeholder="Write New Comment" id="openP1"/></div>
                                          </div>
                                      <div class="form-group row justify-content-center " id="OpenP1" style="display:none;">
                                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                           <button type="button" class="btn btn-outline-success btn1">comment</button>
                                                                     &nbsp; &nbsp; &nbsp;
                                         <button type="button"  class="btn btn-outline-success  btn1" id="cancel1">Cancel</button>
                                      </div>
                             </form>
                              <form action="" method="post" id="textareaP2" style="display:none">
                                          <div class="form-group row">
                                              <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                                                <img src="images/scott_caldwell_4%20(2).png" class="px-2">
                                              </div>
                                               <div class="col-sm-6 col-md-6 col-lg-10 col-xl-10">

                                                   <input type="text" class="form-control comment"  placeholder="Reply This Comment" id="replyP1"/>
                                               </div>
                                          </div>
                                          <div class="form-group row justify-content-center " id="ReplyP1" style="display:none;">
                                                       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                               <button type="button" class="btn btn-outline-success btn1">comment</button>
                                                                         &nbsp; &nbsp; &nbsp;
                                              <button type="button"  class="btn btn-outline-success  btn1" id="cancelReplyP1">Cancel</button>
                                          </div>
                                           </form>
                           </div>
                        </div>                        
                    </div>
                  </div>
             </div>
         
        
               </div>

              <div class="col-sm-12 col-md-4 col-lg-4 col-xl-3 ">   
                for videos and other posts 
              </div> 
        </div>  
        @endforeach    

       </div>
      </div>
  </div>
</div>

   </div><!---end of container-fluid--> 


</div>  
</div>
<script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('jsw/popper.min.js')}}"></script>
  <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('jsw/profile_end.js')}}"></script>


</body>
</html>
