<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>new-profile</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
<link href="{{asset('cssw/follow.css')}}" rel="stylesheet" type="text/css">

</head>
<body>

<div class="l">
    <nav class="navbar  navbar-expand-lg ">
    <div class="container">
      <a class="navbar-brand logo px-2" href="#">SCOUTERIA</a>
      <button class="navbar-toggler open-menu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"  title="open menu">
      </button>
        

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
    
      </div>
        </div>
  </nav>
  </div>
   <!---->


   <div class="container-fluid ">
    <h2>You Have {{$users->unreadNotifications->count()}} Followers Request </h2>
<div class="container first ">

<div class="row">
        @if(count($users))

@foreach($users->unreadNotifications as $notification)

<div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">

    <div class="card ">
 

    <div class="box">
  <div class="img">
            <img src="{{asset('uploads')}}/{{$notification->data['image']}}">

          </div>

         <h2> {{$notification->data['name']}}</h2>
         <p>  {{$notification->data['message']}}</p>

     <form action="{{route('ConfirmFollow')}}">
        <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
             
        <button type="submit"> <a class="btn btn-outline-success" href="{{route('profileuser',$notification->data['user'])}}" >  Accept</a></button>

      </form> 



     <form action="{{route('rejectFollow')}}">
        <input type="hidden" name="user_id" value="{{$notification->data['user']}}">       
        <button type="submit" class="btn btn-outline-success"><a class="btn btn-outline-success" href="{{route('profileuser',$notification->data['user'])}}" >reject</a></button>

      </form> 

    </div>



   </div>
  
    </div>    <br>  
   @endforeach
   @endif 
    <div class="line mt-3"></div>  <br>


    </div>
 
        </div>

    </div>

 






<script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('jsw/popper.min.js')}}"></script>
<script src="{{asset('jsw/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('jsw/follow.js')}}"></script>
</body>
</html>
