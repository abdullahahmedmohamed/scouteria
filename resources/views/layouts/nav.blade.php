<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>profile</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
<link href="{{asset('cssw/profile_end.css')}}" rel="stylesheet" type="text/css">
@yield('style')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <div id="all">
<nav class="navbar navbar-expand-lg nav-color mb-4" >
<div class="container px-2">
  <a class="navbar-brand logo  offset-1" href="#">SCOUTERIA</a>
  <button class="navbar-toggler togg" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" title="open-menu">
    <span class="navbar-toggler-icon"></span>
  </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto ">
                 

      <li class="nav-item  search ">
          <span class="fa fa-search search-icon px-3 py-2">
          </span>
            <input class="form-control br-40   px-5 " type="search" placeholder="Search" aria-label="Search">
        </li> 
         &nbsp; &nbsp;
               <li class="nav-item ">
                  <a class="nav-link " href="#" title="profile">
                   <img src='#' class="img ">    
                     <span class="name" title="profile">{{$users->name}}</span></a>                 
         </li>  
        
          &nbsp; &nbsp;&nbsp;
                <li class="nav-item  ">
                 <a  href="#" title="Home">
                   <i class="fa fa-fw fa-home home"></i> 
                     </a> 
         </li>
           &nbsp; &nbsp;&nbsp;
               <li class="nav-item  dropdown">
                 <a  href="#" title="Message" id="button3">
                   <i class="fa fa-fw fa-envelope home"><sup class="count">10</sup></i> 
                  <div class="dropdown-menu popover fade show bs-popover-top " 
           role="tooltip" id="item3" x-placement="bottom"  style="display:none;">
             <ul class=" px-3 mb-2">
                 <li>
                     <a href="" class="msg">Recent(13)
                     </a>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="" class="msg">Messages Request
                     </a>
                 </li>
                      </ul>
          <div class="EaRLIER"><p>EARLIER</p></div>
                 <div class="popover-body">
            <ul class="unstyled px-1">
    <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

    <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
    <p  style="text-align:left">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix"></div>
      </li><br>
                        <div class="noti-footer mb-5 py-4">Read more</div>

           <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

   <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

            <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

  <li data-alert_id="1" class="alert_li py-3 ">
        <a href="">
            <img src="./images/scott_caldwell_4.png" class="notification-img " />
            <span class="noti_message">  Hecham Ahmed</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>11:33 Am</span>
            
            <p  style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &#10004;   welcome to scouteria
            </p>
               </a>
        <div class="clearfix "></div>
      </li>

</ul></div></div>   
    
                     
                             
                     
                     
                     
                     </a> 
         </li>  
         &nbsp; &nbsp;&nbsp;
        
        <li class="nav-item dropdown">  
       <a  href="#" title="NOTIFICATIONS" id="button" 
          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  
         <i class="fa fa-fw fa-bell home"><sup class="count2">{{$users->unreadNotifications()->where('data->type','confirmed','rejected')->get()->count()}}</sup></i> 
           </i>

<!--this is div for notification -->

      <div class="dropdown-menu popover fade show bs-popover-top " 
           role="tooltip" id="item" x-placement="bottom"  style="display:none;">
             <ul class=" px-1 ">
                 <li style="text-align:center;" ><p class="msg">NOTIFICATIONS</p></li>
             </ul>

             <div class="EaRLIER2 mb-3 "><p class="msg">EARLIER</p></div>
                 <div class="popover-body">
                    <ul class="unstyled px-1 mt-1">
                        @foreach($users->unreadNotifications()->where('data->type','confirmed','rejected')->get() as $notification)
                         <li data-alert_id="1" class="alert_li mb-2">
                            <a href="{{route('profileuser',$notification->data['user'])}}">
                            <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="notification-img  " ></a>
                            <a href="{{route('profileuser',$notification->data['user'])}}" class="alert_message">
                                {{$notification->data['message']}}
                             </a>
                             <br>
                              <a href="#" class="time_alert">
                                {{$notification->created_at}}
                              </a>
                            <div class="clearfix"></div><br>
                         </li>
                            @endforeach 

                         <br>

                         <div class="noti-footer mb-5">Read more</div>
                    </ul>
                 </div>
      </div>



        </a>  </li>
    
         &nbsp; &nbsp;&nbsp;
        <li class="nav-item dropdown">  
       <a  href="#" title="Followers" id="button2"
       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  
        <i class="fa fa-fw fa-user home"><sup class="count">{{$users->unreadNotifications()->where('data->type','send_request')->get()->count()}}</sup>
           </i>
      <div class="dropdown-menu popover fade show bs-popover-top " 
           role="tooltip" id="item2" x-placement="bottom"  style="display:none;">
             <ul class=" px-1 py-1">
               <li style="text-align:center;" class="msg">Follow Requests</li>
          </ul>
          <div class="EaRLIER "><p>EARLIER</p></div>
                 <div class="popover-body">
  <ul class="unstyled px-1">

    @foreach($users->unreadNotifications()->where('data->type','send_request')->get() as $notification)

       <li data-alert_id="1" class=" py-2">
         <div class="alert_li2">
           <a href="{{route('profileuser',$notification->data['user'])}}" >
               <img src="{{asset('uploads')}}/{{$notification->data['image']}}" class="notification-img  " >
           </a>
            <a href="#" class="request alert_li2">{{$notification->data['name']}} <span>{{$notification->data['message']}}</span>
            </a>
          </div>
           <div class="container">
        <div class="row justify-content-end">

             
        <form action="{{route('ConfirmFollow')}}">
        <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
        <input type="hidden" name="id" value="{{$notification->id}}"> 
              <button type="submit" title="Accept" class="Accept"><a href=""> 
                              <i class="far fa-check-circle "> </i>
                                </a>
                        </button>
                   </form>
                    &nbsp;&nbsp;
             <form action="{{route('rejectFollow')}}">
                 <input type="hidden" name="user_id" value="{{$notification->data['user']}}">
                 <input type="hidden" name="id" value="{{$notification->id}}">

            <button type="submit" title="Reject"><i class="far fa-times-circle"></i></button>
            </form>
        </div> 
            </div>

 
               <div class="clearfix"></div>
                   @endforeach    
                    <br>
               <div class="noti-footer mb-5 mm">Read more</div>
          </ul>
        </div>
        </div>

           
                  
            </a>     

    
      &nbsp; &nbsp; 
            
        <li class="nav-item  dropdown">
                 <a  href="{{route('logout')}}" title="Settings" >
              <i class="fa fa-fw fa-cog home"></i>
                     </a> 
         </li>  
        </ul>
    </div>  
            </div>
</nav>  
@yield('content') 

@yield('scripts')


    </div>  
<script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('jsw/popper.min.js')}}"></script>
  <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('jsw/profile_end.js')}}"></script>


</body>
</html>