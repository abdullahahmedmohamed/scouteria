<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Home Page</title>
<link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
<link href="{{asset('cssw/listfollow.css')}}" rel="stylesheet" type="text/css">


</head>
<body >



<div class="l">
    <nav class="navbar  navbar-expand-lg ">
    <div class="container">
      <a class="navbar-brand logo px-2" href="#">SCOUTERIA</a>
      <button class="navbar-toggler open-menu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"  title="open menu">
      </button>
        

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
    
      </div>
        </div>
  </nav>
  </div>
    
  <div class="container-fluid">
<div class="container mt-3">

    <!--this is links for following-->
        <ul class="nav nav-tabs nav-justified md-tabs" id="myTabJust" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="Followers-tab-just" data-toggle="tab" href="#Followers-just" role="tab" aria-controls="Followers-just" aria-selected="true">Followers</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="Following-tab-just" data-toggle="tab" href="#Following-just" role="tab" aria-controls="Following-just" aria-selected="false">Following</a>
  </li>
            
            <li class="nav-item">
    <a class="nav-link" id="pendding-tab-just" data-toggle="tab" href="#pendding-just" role="tab" aria-controls="pendding-just" aria-selected="false">Pendding</a>
  </li>
        
             
</ul>
<div class="tab-content card2 px-4 py-4" id="myTabContentJust">
  <div class="tab-pane fade show active" id="Followers-just" role="tabpanel" aria-labelledby="Followers-tab-just">
    
<div class="container first ">
 <div class="row">
          @if(count($followers)) 
          @foreach($followers as $follower)
  <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
       <div class="card ">
          <div class="box">
            <a href="#">
              <div class="img">
                  <img src="{{asset('uploads')}}/{{$follower->image}}">

                </div></a>
<a href="{{route('profileuser',$follower->id)}}"><h2>{{$follower->name}} </h2></a> 
              <p> Wants To Follow You.</p>
           <div class="container ">
             <div class="row justify-content-center">
                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                        <form action="{{route('unFollow')}}" method="post">
                            @csrf
                  <input type="hidden" name="user" value="{{$follower->id}}"> 

                           <button  class="btn btn-outline-success  btn-custom"type="submit">Unfollow</button>
                                  
                          </form>
                 </div> 

               </div>  
            </div>

          </div>
        </div>
          
    </div>           @endforeach
           @endif 
 </div>

 </div>

 <br>
        <div class="line mt-3"></div>

</div>
  
    
    
    
    
    
    
    
    
    
          <!--this is second link following-->
    
    
<div class="tab-pane fade" id="Following-just" role="tabpanel" aria-labelledby="Following-tab-just">
 <div class="container first ">
   <div class="row">
        @if(count($following))
          @foreach($following as $follow)
    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
      <div class="card ">
        <div class="box">
          <a href="#">
            <div class="img">
                <img src="{{asset('uploads')}}/{{$follow->image}}">

             </div></a>
           <a href="{{route('profileuser',$follow->id)}}"> <h2>{{$follow->name}}</h2> </a>
            <p> Wants To Follow You.</p>
            <div class="container ">
             <div class="row justify-content-center">
              <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                <form action="{{route('unFollow')}}" method="post">
                    @csrf
                  <input type="hidden" name="user" value="{{$follow->id}}"> 
                  <button  class="btn btn-outline-success  btn-custom" type="submit">Unfollow</button>
                         
                </form>
              </div> 

             </div>  
            </div>
         </div>
      </div>
    
    </div> 
          @endforeach
        @endif    

    
    </div>
    </div>
  
    <br>
    <div class="line mt-3"></div>

  </div>
  
    
    
    
    
    
    
    
    
    
    
    
    
          <!--this is second link pendding-->
    
    
    
    
    <div class="tab-pane fade" id="pendding-just" role="tabpanel" aria-labelledby="pendding-tab-just">
    <div class="container first ">
<div class="row">

@foreach($users->unreadNotifications()->where('data->type','send_request')->get() as $notification)
<div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
    <div class="card ">
    <div class="box">
        <div class="img">
            <img src="{{asset('uploads')}}/{{$notification->data['image']}}">

          </div>
        <h2>{{$notification->data['name']}}</h2>
        <p>  {{$notification->data['message']}}</p>
      <div class="container ">
        <div class="row ">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
              <form action="{{route('ConfirmFollow')}}">
        <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 
        <input type="hidden" name="id" value="{{$notification->id}}"> 


             <button  class="btn btn-outline-success  btn-custom1" type="submit">
              Accept</button>
                        
             </form>
           </div> 
          <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
              <form action="{{route('rejectFollow')}}">
                 <input type="hidden" name="user_id" value="{{$notification->data['user']}}"> 

                   <button  class="btn btn-outline-success  btn-custom2  " type="submit">
                    Reject  
                  </button>
                   
              </form> 
          </div> 
       </div>  
      </div>

    </div>
</div>
    
    </div> 
    
   @endforeach
    
    </div>
   </div><br>
                <div class="line mt-3"></div><br>
          
    
    
    
         
                
                
        </div>
       </div>
    
    
    
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


    

<script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('jsw/popper.min.js')}}"></script>
<script src="{{asset('jsw/bootstrap.min.js')}}"></script>
</div>
</body>
</html>