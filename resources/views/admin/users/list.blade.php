@extends('admin.layout.app')

@section('title')
Users
@endsection
@section('content')

 <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Data </h3>

            </div>



            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                   <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>  </th>

                </tr>
                </thead>
                <tbody>

                  @if(count($users))

                      @foreach($users as $user)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <!--
                         <td>
                          <a href="{{route('delete_user',$user->id)}}" class="btn btn-danger"> <i class="fa fa-close"></i></a>
                        </td>
                        -->
                      </tr>
                     @endforeach
                    @endif
              
                
               
                  
                </tfoot>
              </table>
<!--
            <ul class="pagination">
<li class="page-item disabled">
<a class="page-link" href="#">Previous </a></li>
<li class="page-item"><a class="page-link" href="#">1</a></li>
<li class="page-item"><a class="page-link" href="#">2</a></li>
<li class="page-item"><a class="page-link" href="#">3</a></li>
<li class="page-item"><a class="page-link" href="#">Next</a></li>
</ul>
-->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

@endsection