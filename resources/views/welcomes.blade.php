<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>SCOUTERIA</title>
      <link rel="stylesheet" href="{{asset('cssw/bootstrap.min.css')}}">
      <link href="{{asset('cssw/welcome.css')}}" rel="stylesheet" type="text/css">
   </head>
   <body>
  <div class="l">
      <nav class="navbar  navbar-expand-lg color-nav">
         <div class="container">
            <a class="navbar-brand logo" href="#">SCOUTERIA</a>
            <button class="navbar-toggler open-menu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"  title="open menu">
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
               </ul>
               
                  <div class="user-image">      
                  <img src="{{asset('images/union_12.png')}}" class="img-fluid user" >
                  </div>
                
                  <form class="form-inline my-2 my-lg-0 " method="POST" action="{{route('login')}}">
                  @csrf
                  <input class="form-control br-40 mr-sm-2 py-2 search" type="search" placeholder="Email or phone" aria-label="Search" style="height:30px; line-height:30px; "  name="email" required>
                  <input class="form-control br-40 mr-sm-2 py-2   search" type="password" name="password" placeholder="Enter password" aria-label="Search" style="height:30px; line-height:30px" required>
                  
                    <button type="submit" class="my-2 my-sm-0 log-in " >log In</button>
                  
                 </form>
                <a href="{{route('reset_link')}}"> forget password </a>
            </div>
         </div>
      </nav>

   </div>   
      <div class="container-fluid second " >
         <div class="container "  >
            <div class="row " >
               <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
                  <div class="container fix">
                     <div class="row ">
                        <div class="color-div">
                           <br>
                           <h6 class="create px-2" > &nbsp;Create a new account</h6>
                           <h6 class=" free px-3">It's free and always will be.</h6>
                           <div class="px-3 py-1 " >
                              <form onsubmit="return validate()" name="myForm" action ="{{route('registration')}}" method="POST">
                                 @csrf
                                 <div class="form-row ">
                                    <input type="Name"  class="form-control br-40 " placeholder="User Name" id="name" name="name" required>
                                    <span style=' color: red ; font-size: 25px;' id="test">
                                    </span>
                                    <span class="px-2  py-2 Error" id="errorname"> </span>
                                 </div>
                                 <div class="form-row " >
                                    <input  type="email" class="form-control  br-40 " placeholder="Email"  name="email" class="form-control" id="email" >
                                    <span class="px-2 py-2 Error" id="verifyemail"></span>
                                 </div>
                                 <div class="form-row ">
                                    <input type="Password" class="form-control  br-40 " placeholder="Password" name="password" id="password"> 
                                    <span class="px-2 py-2 Error" id="errorpassword"></span>
                                 </div>
                                 <div class="form-row">
                                    <select name="sport_id" class="custom-select select_join  br-40"  id="sport">
                                       <option >select Your Sport</option>
                                       @foreach($sports as $sport)
                                       <option value="{{$sport->id}}">
                                          {{$sport->sport}}
                                       </option>
                                       @endforeach
                                    </select>
                                    <span class="px-2 py-2 Error" id="errorsport"></span>
                                 </div>
                                 <div class="form-row">
                                    <select name=type_id  class="custom-select select_join  br-40" id="type">
                                       <option >select who you are</option>
                                       @foreach($type as $types)
                                       <option value="{{$types->id}}">
                                          {{$types->type}}
                                       </option>
                                       @endforeach
                                    </select>
                                    <span class="px-2 py-2 Error" id="errortype"></span>
                                 </div>
                                 <div class="form-row">
                                    <select class="custom-select select_join  br-40"  name="country_id" id="country">
                                       <option>select your country</option>
                                       @foreach($countries as $country)
                                       <option value="{{$country->id}}">
                                          {{$country->name}} 
                                       </option>
                                       @endforeach
                                    </select>
                                    <span class="px-2 py-2 Error" id="errorcountry" ></span>
                                 </div>

                                 <div class="form-row mb-3">  
                                      @foreach($genders as $gender)
                                      &nbsp;&nbsp;&nbsp;&nbsp;       
                                            &nbsp;&nbsp;&nbsp;&nbsp; 
                                      <div class="radio-item ">
                                      <input type="radio" id="ritema{{$gender->id}}" name="gender_id" 
                                      value="{{$gender->id}}" checked>
                                      <label for="ritema{{$gender->id}}" class="male">{{$gender->gender}}</label>
                                  </div>
                                                                             @endforeach
                                  </div>
                                 <div class="form-row px-2">
                                    <button id="sign_up" class="btn btn-block br-40 btn-custom mb-2 px-5"  name="Sign up">Sign up</button>
                                 </div>
                            <div class="form-row mb-2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                    
                                 <div class="social">
                                    <a href="{{route('facebook_login')}}" class="facebook">
                                       <svg viewBox="0 0 33 33">
                                          <g>
                                             <path d="M 17.996,32L 12,32 L 12,16 l-4,0 l0-5.514 l 4-0.002l-0.006-3.248C 11.993,2.737, 13.213,0, 18.512,0l 4.412,0 l0,5.515 l-2.757,0 c-2.063,0-2.163,0.77-2.163,2.209l-0.008,2.76l 4.959,0 l-0.585,5.514L 18,16L 17.996,32z"></path>
                                          </g>
                                       </svg>
                                    </a>
                                    <a href="{{route('twitter_login')}}" class="twitter">
                                       <svg viewBox="0 0 33 33">
                                          <g>
                                             <path d="M 32,6.076c-1.177,0.522-2.443,0.875-3.771,1.034c 1.355-0.813, 2.396-2.099, 2.887-3.632 c-1.269,0.752-2.674,1.299-4.169,1.593c-1.198-1.276-2.904-2.073-4.792-2.073c-3.626,0-6.565,2.939-6.565,6.565 c0,0.515, 0.058,1.016, 0.17,1.496c-5.456-0.274-10.294-2.888-13.532-6.86c-0.565,0.97-0.889,2.097-0.889,3.301 c0,2.278, 1.159,4.287, 2.921,5.465c-1.076-0.034-2.088-0.329-2.974-0.821c-0.001,0.027-0.001,0.055-0.001,0.083 c0,3.181, 2.263,5.834, 5.266,6.438c-0.551,0.15-1.131,0.23-1.73,0.23c-0.423,0-0.834-0.041-1.235-0.118 c 0.836,2.608, 3.26,4.506, 6.133,4.559c-2.247,1.761-5.078,2.81-8.154,2.81c-0.53,0-1.052-0.031-1.566-0.092 c 2.905,1.863, 6.356,2.95, 10.064,2.95c 12.076,0, 18.679-10.004, 18.679-18.68c0-0.285-0.006-0.568-0.019-0.849 C 30.007,8.548, 31.12,7.392, 32,6.076z"></path>
                                          </g>
                                       </svg>
                                    </a>
                                    <a href="{{route('linkedin_login')}}" class="linkedin">
                                       <svg viewBox="0 0 512 512">
                                          <g>
                                             <path d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"></path>
                                          </g>
                                       </svg>
                                    </a>
                                 </div>
                           </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-7">
               <div class="container">
                  <div class="row">
                     <h1 class="connecting ">Connecting</h1>
                  </div>
                  <div class="row px-5">
                     <h6 class="the-world">The World's Athletes<br> To Take Them To <br> The Next Level</h6>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      </div>
      <script type="text/javascript" src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
      <script type="text/javascript">

        /*$('#name').on('focusout',function(){
            var ne=document.forms["myForm"]["name"];
            var errorname=  document.getElementById("errorname");
            var test=  document.getElementById("test");
            var letters = /^[A-Z a-z]+$/;
          
            if(! ne.value.match(letters))
                 
             {
                errorname.innerHTML="Please provide your Username characters only!";
                  test.innerHTML=" &#9432;";
                  ne.style.border="2px solid red";
                 return false;
             }
              else    
              {
                errorname.innerHTML="";
                test.innerHTML="";
                ne.style.border="2px solid green";
                 return false;
             }  


        });*/
         $('#email').on('focusout',function(){
         
              /*Will Be Refactored Later*/
                 var x=document.forms["myForm"]["email"];
                  var g=document.forms["myForm"]["email"].value;
                  var atpos=g.indexOf("@");
                  var dotpos=g.lastIndexOf(".");
                  var verifyemail=  document.getElementById("verifyemail");
         
                  if (atpos<1 || (dotpos-atpos)<=1 || dotpos>=g.length-2)
                  {
                    verifyemail.innerHTML=" &#9432; Please input Valid email address";
                    x.style.border="2px solid red";
                   $('#sign_up').prop('disabled', true);
                   return false;
         
                   }else{
                      var email = $(this).val().trim();
                        $.ajax({
                      "type" : "GET",
                      "url" : "{{route('ajax')}}",
                      "data" : {email:email},
                      "success": function(response){
         
                        if (response.response == 0){
                            $('#verifyemail').html('There is an existing account associated with email');
                            $('#sign_up').prop('disabled', true);
                   return false;
                          }
                            else{
         
                                $('#email').css('border', '1px solid green');
                                $('#verifyemail').html('');
                                $('#sign_up').prop('disabled', false);
         
                             }
                            }
                        
                        });
         
                   }
         
         
         });
      </script>
      <script src="{{asset('jsw/jquery-3.3.1.min.js')}}"></script>
      <script src="{{asset('jsw/popper.min.js')}}"></script>
      <script src="{{asset('jsw/bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('jsw/welcome.js')}}"></script>
   </body>
</html>