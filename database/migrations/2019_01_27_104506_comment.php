<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('comment'))
            {
                Schema::create('comment', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('user_id');
                    $table->integer('post_id');
                    $table->longText('text')->nullable();
                    $table->timestamps();

               });
            }    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
