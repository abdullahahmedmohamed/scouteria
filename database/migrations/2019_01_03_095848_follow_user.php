<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FollowUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('follow_user'))
            {
               Schema::create('follow_user', function (Blueprint $table) {
                   $table->increments('id');
                   $table->integer('user_id');
                   $table->integer('followed_user_id');
                   $table->timestamps();
               });          
            }


   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
