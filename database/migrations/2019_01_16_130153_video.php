<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Video extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('video')){

        Schema::create('video',function(Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('user_id');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
