<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('media')){
        Schema::create('media',function(Blueprint $table){
            $table->increments('id');
            $table->string('media_path');
            $table->tinyInteger('user_id');
            $table->tinyInteger('media_type_id');
            $table->timestamp('deleted_at');
            $table->timestamps();
   
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
