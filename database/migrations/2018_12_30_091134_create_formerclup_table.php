<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormerclupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           if(!Schema::hasTable('formerclub')){

        Schema::create('formerclub', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
        });
    }
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formerclup');
    }
}
