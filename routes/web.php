<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login/facebook', 'socialloginController@redirectToProvider')->name('facebook_login');

Route::get('login/facebook/callback', 'socialloginController@handleProviderCallback');
//TWITTER

Route::get('login/twitter', 'socialloginController@redirectToTwitterProvider')->name('twitter_login');
Route::get('login/twitter/callback', 'socialloginController@handleTwitterProviderCallback');
//LINKEDIN
Route::get('login/linkedin', 'socialloginController@redirectToLinkedInProvider')->name('linkedin_login');
Route::get('login/linkedin/callback', 'socialloginController@handleLinkedInProviderCallback');


//Route::get('/', function () {  return view('welcome');});
// route:: resource('user','User' );

//Auth::routes();
//Route::post('login', 'user@login')->name('login');

Route::get('/home', 'ProfileController@index')->name('home');
Route::get('/profile','ProfileController@index')->name('profile');
Route::get('/profileuser/{id}','ProfileController@userProfile')->name('profileuser');
Route::post('write','ProfileController@writePost')->name('writePost');
Route::get('remove/{id}','ProfileController@removePost')->name('removePost');
Route::post('edit/{id}','ProfileController@changePost')->name('changePost');
Route::get('/editpage','ProfileController@editpage')->name('editpage');
Route::get('/editPost/{id}','ProfileController@editPost')->name('editPost');

Route::get('/doneregestration', 'welcomecontroller@doneregestration')->name('doneregestration');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
Route::get('/password/form', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('reset_link');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
//ADSasdasd

///////////////////////
Route::post('edit','ProfileController@edit')->name("editinfo");
Route::post('editbirthday','ProfileController@editBirthday')->name("editBirthday");
Route::post('editsport','ProfileController@editSport')->name("editSport");
Route::post('editcountry','ProfileController@editCountry')->name("editCountry");
Route::post('editheight','ProfileController@editHeight')->name("editHeight");
Route::post('editweight','ProfileController@editWeight')->name("editWeight");
Route::post('achievments','ProfileController@achievments')->name("achievments");
Route::post('city','ProfileController@city')->name("city");
Route::post('academy','ProfileController@academy')->name("academy");




Route::post('editpp','ProfileController@editpp')->name("editimg");
Route::post('saveclub','ProfileController@saveclub')->name("saveclub");
Route::get('saveformerclub','ProfileController@saveformerclub')->name("saveformerclub");

//////////////////////

Route::group(['prefix' => 'admin'], function () {
Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
Route::post('/login', 'AdminAuth\LoginController@login');
Route::post('/logout', 'AdminAuth\LoginController@logout');

Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
Route::post('/register', 'AdminAuth\RegisterController@register');

Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
////////start hare 
 Route::get('/','welcomecontroller@index')->name('welcome');
 Route::get('/email','welcomecontroller@email')->name('email');
 Route::get('/list','welcomecontroller@listuser')->name('listuser');
 Route::get('/list/delete/{id}','welcomecontroller@delete_user')->name('delete_user');

Route::post('registration','welcomecontroller@registration')->name('registration');
Route::get('ajax','welcomecontroller@ajax')->name('ajax');
Route::get('/test',function(){
	return view('test');
});


//////////// to follow 

Route::get('/showfreinds','followController@index')->middleware('auth')->name('showfreinds');
Route::post('followuser','followController@follow_user')->name("follow_user");
Route::post('unfollow','followController@unFollow')->name("unFollow");
Route::get('/followers','followController@MyFollowing')->name("followers");

Route::get('/ConfirmFollow','followController@ConfirmFollow')->name("ConfirmFollow");
Route::get('/rejectFollow','followController@rejectFollow')->name("rejectFollow");
Route::get('/readNotification','ProfileController@readNotification')->name("readNotification");

Route::get('/newsfeed','ProfileController@newsFeed')->name('newsfeed');

Route::get('/listfollowers','followController@listFollowers')->name('listfollowers');
Route::get('/notifications','notifyControllery@listNotification')->name('listNotification');

Route::get('/like','followController@like')->name("like");
Route::get('/unlike','followController@unLike')->name("unLike");
Route::get('/writecomment','followController@writeComment')->name("writeComment");
Route::get('/editcomment/{id}','followController@editComment')->name("editComment");
Route::get('/removecomment/{id}','followController@removeComment')->name("removeComment");


////////////