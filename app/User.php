<?php

namespace App;
use App\likes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\FollowUser;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_verified_at','biography',
        'available','status','type_id', 'gender_id','country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $table = 'users';


          public function type()
            {
                return $this->belongsTo('App\type');
            
            }

          public function gender()
            {
                return $this->belongsTo('App\gender');
            
            }

          public function country()
            {
                return $this->belongsTo('App\geo_countries');

            
            }
          public function sport()
            {
                return $this->belongsTo('App\sport');

            
            }

          public function club()
            {
                return $this->belongsTo('App\club');

            
            }
          public function clubformer()
            {
                return $this->belongsTo('App\clubformer');

            
            }
          // public function follow(){

          //        return $this->hasMany('App\follow_user');

          //   }

            public function following()
            {
              return $this->belongsToMany('App\User', 'follow_user', 'user_id', 'followed_user_id')->withTimestamps();
            }

            // Same table, self referencing, but change the key order
            public function followers()
            {
              return $this->belongsToMany('App\User', 'follow_user', 'followed_user_id', 'user_id')->withTimestamps();
            }
    // public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new ResetPasswordNotification($token));
    // }

            
    public function posts()
    {
        return $this->hasMany('App\post','user_id');
    }
    public function video()
    {
        return $this->hasMany('App\video','user_id');
    }

 

  public function ThisUserInMyFollow($id)
  {
     $in_my_followers= $this->followers()->where('users.id',$id)->first();
     if($in_my_followers)
     {

          return 'followed';
     }else{

      $following = FollowUser::where('user_id',$this->id)->where("followed_user_id",$id)->first();
        if(is_null($following))
        {
          return 'not_followed';
        }else{
          if($following->confirmed == 0)
          {
            return 'pending';
          }elseif($following->confirmed == 1)
          {
            return 'followed';

          }else{

            return 'not_followed';
          }

        }
     } 

  }

        public function like(){

          return $this->hasMany('App\likes');
        }

        public function comment(){

          return $this->hasMany('App\comment');
        }

   }
