<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class users extends Model
{

    protected $table = 'users';
  //  public $timestamps= false;

          public function type()
            {
                return $this->belongsTo('App\type');
            
            }

          public function gender()
            {
                return $this->belongsTo('App\gender');
            
            }

          public function country()
            {
                return $this->belongsTo('App\country');

            
            }
          public function sport()
            {
                return $this->belongsTo('App\sport');

            
            }

          public function club()
            {
                return $this->belongsTo('App\club');

            
            }
          public function clubformer()
            {
                return $this->belongsTo('App\clubformer');

            
            }

            public function follow_user(){

              return $this->hasMany('App\follow_user');
            }
}