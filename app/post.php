<?php

namespace App;
use App\likes;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{

 protected $table ='post';


        public function postmedia(){

          return $this->hasMany('App\media');
        }
        public function lastVideo()
        {
        	return $this->postmedia()->where('media_type_id','2')->orderBy('created_at','DESC')->first();
        }

        public function like(){

        	return $this->hasMany('App\likes');
        }

        public function countlike()
        {
            return $this->like()->where('like','1')->get()->count();
        }

        public function comment(){

          return $this->hasMany('App\comment');
        }    

}
