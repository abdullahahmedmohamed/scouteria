<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
class FollowNotification extends Notification
{
    use Queueable;

    public $notification_body =array();
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notification_body)
    {
        return $this->notification_body = $notification_body;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return $this->notification_body;
                   
    }

/**
*
*to make real time notification
*/

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage ( $this->notification_body);
                   
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
