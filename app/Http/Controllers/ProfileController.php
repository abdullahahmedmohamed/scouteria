<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\type;
use App\gender;
use App\geo_countries;
use App\sport;
use App\club;
use App\formerclub;
use App\post;
use App\FollowUser;
use App\notifications;
use App\media;
use App\likes;
use App\comment;
use Validator;
use App\Notifications\FollowNotification;
use Notification;
use Hash;
use Illuminate\Support\Facades\Mail;
class ProfileController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

        public function index()
    {
    	$users = Auth::user();
    	$id =  Auth::id();
    	$user = user::all();
    	$sports = sport::all(); 
    	$clubs = club::all();
    	$comment = comment::all();
    	$countries = geo_countries::all();
   		//$userlikes =  $users->like()->where('user_id',Auth::id())->get();
    	// $name =  $user->comment()->orderBy('created_at','DESC')->get();
    	$posts =  $users->posts()->orderBy('created_at','DESC')->get();
		$following = Auth::user()->following()->where('confirmed',1)->get();
		$followers = Auth::user()->followers()->where('confirmed',1)->get();
		// $notifications_request = $users->unreadNotifications()->where('data->type','send_request')->orderBy('created_at','DESC')->get();
		// $notifications_news = $users->unreadNotifications()->where('data->type','confirmed')->orderBy('created_at','DESC')->get();
	



		// dd($users->unreadNotifications);
		$notifications_request= [];
		foreach ( $users->unreadNotifications as $not) {
			if($not->data['type'] == 'send_request'){
				array_push($notifications_request,  $not);
			}
	
		}
		// // echo "<pre>";
		$notifications_news= [];
		foreach ( $users->unreadNotifications as $not) {
			if($not->data['type'] == 'confirmed'){
				array_push($notifications_news,  $not);
			}
		
		}
		// echo "</pre>";

		// $userNotification =$notifications_request->data['user'];
    	$last_post = $users->posts()->orderBy('created_at','DESC')->first();
    	$last_video = 0;
    	$all = User::all();    	

    	
			        
    	if(!is_null($last_post))
    	{
    		$last_video = $last_post->lastVideo();

    	}

		
    	 return view('profile',compact('users','last_video' ,'likes' , 'userlikes','followers','following','posts','comment','id','all','notifications_request','notifications_news','userNotification'));
    }
        public function userProfile($id)
    {
    	$users = Auth::user();
    	$following =User::findOrFail($id)->following()->where('confirmed',1)->get();
		$followers = User::findOrFail($id)->followers()->where('confirmed',1)->get();
    	$freinds = User::findOrFail($id);
    	$userlikes =  $freinds->like()->where('user_id',Auth::id())->get();
    	$follow_status = Auth::user()->ThisUserInMyFollow($id);
    	$id =  Auth::id();
    	$all = User::all(); 
    			// dd($users->unreadNotifications);
		$notifications_request= [];
		foreach ( $users->unreadNotifications as $not) {
			if($not->data['type'] == 'send_request'){
				array_push($notifications_request,  $not);
			}
	
		}
		// // echo "<pre>";
		$notifications_news= [];
		foreach ( $users->unreadNotifications as $not) {
			if($not->data['type'] == 'confirmed'){
				array_push($notifications_news,  $not);
			}
		
		}
		// $userNotification =$notifications_request->data['user'];

//		$users->unreadNotifications()->update(['read_at' => now()]);
		return view('profileUser',compact('freinds','follow_status', 'userlikes','users','followers','following','id','all','notifications_request','notifications_news','userNotification'));
    }
		public function readNotification(Request $request)
	{
    	$notification_id =  $request->id; 
    	$user= Auth::id();
    	$users= User::findOrFail($user);
		$users->unreadNotifications()->where('id',$notification_id)->update(['read_at' => now()]);
  		return redirect()->route('profile');		

		
	}
    public function editpage(){
    	$sports = sport::all();
    	$clubs = club::all();
    	$formerclubs = formerclub::all();
    	$countries = geo_countries::all();
    	$users = Auth::user();


    	return view('edit',compact('sports','clubs','formerclubs','countries','users'));

    }

	public function edit(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->name = $request->name;
		// $users->sport_id = $request->sport;

		// $users->club_id = $request->club;
		// $users->formerclub_id =0;

		$users->save();
		return redirect()->route('editpage');

	}
	public function editBirthday(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		 $users->birthday = $request->birthday;

		$users->save();
		return redirect()->route('editpage');

	}
	public function editSport(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->sport_id = $request->sport;

		$users->save();
		return redirect()->route('editpage');

	}
	public function editCountry(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->country->id = $request->country;

		$users->save();
		return redirect()->route('editpage');

	}

	public function editHeight(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->height = $request->height;
		$users->save();
		return redirect()->route('editpage');

	}

	public function editWeight(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->weight = $request->weight;	

		$users->save();
		return redirect()->route('editpage');

	}

	public function editpp(Request $request)
	{
		

			$users = Auth::user();

			$image = $request->file('image');
			if($image){
			$imagee = $image->getClientOriginalName();
			$image->move('uploads',$imagee);
			$users->image = $imagee;
			$users->save();
			return redirect()->route('profile');
		}
		else{
			return redirect()->route('profile');
		}
	}
	public function saveclub(request $request)
	{
			$users = Auth::user();
			$users->club_id = $request->club;
			$users->save();
			return redirect()->route('editpage');


	}

	public function saveformerclub(request $request)
	{

		$formerclub = new formerclub ;
		$formerclub->name = $request->name;
		$formerclub->save();

	}
		public function achievments(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->achievments = $request->achievments;
		$users->save();
		return redirect()->route('editpage');

	}
		public function city(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->city = $request->city;
		$users->save();
		return redirect()->route('editpage');

	}

		public function academy(Request $request )
	{
		//TODO: add Validation 
		$users = Auth::user();
		$users->academy = $request->academy;
		$users->save();
		return redirect()->route('editpage');

	}

	public function writePost(Request $request)
	{
		//dd($request->all());
		$post = new post;
		$post->status = 0;
		$post->user_id = Auth::id();
		$post->text = $request->input('body');
		$post->save();
    	// $likes = $post->like();
    	// if(!is_null($countlike)){
    	// 	$likes = countlike();
    	// }		if($request->hasFile('image'))
		{
			//read the file
			$media = new media;
			$image = $request->file('image');
			if($image){
			$image_path = str_random(10).'.'.$image->extension();
			$extension = $image->extension();
			$image_extensions = ['jpeg','png','gif','tiff'];
			$image->move('imgPosts',$image_path);
			$media->media_path = $image_path;
			$media->post_id = $post->id ;

			// $media->media_type_id=1;

			$validator = Validator::make($request->all(), [
			    'file' => 'max:500000',
			]);
			if($validator->fails())
			{
				return redirect()->back();
			}


			// dd($validator->errors());
			if(in_array($extension,$image_extensions)){
					$media->media_type_id=1;
				}
				else{
					$media->media_type_id=2;
				}
				
			$media->save();
}
			return redirect()->route('profile');
		}
	}

 
	public function removePost($id)
	{
		//dd($request->all());
		
		$post = post::findOrFail($id)->delete();
			return redirect()->route('profile');

	}

	public function changePost(Request $request , $id )
	{
		//dd($request->all());
		
			
  			$post= post::findOrFail($id);
			$post->text = $request->input('body');
			$post->save();
			// dd($post);

			$idimg = $request->input('idimg');
			$media = media::findOrFail($idimg);
			$image = $request->file('image');
			if($image){
			$image_path = str_random(10).'.'.$image->extension();
			$extension = $image->extension();
			$image_extensions = ['jpeg','png','gif','tiff'];
			$image->move('imgPosts',$image_path);

			//read the file
			$media->media_path = $image_path;
			$validator = Validator::make($request->all(), [
			    'file' => 'max:500000',
			]);
			if($validator->fails())
			{
				return redirect()->back();
			}


			// dd($validator->errors());
			if(in_array($extension,$image_extensions)){
					$media->media_type_id=1;
				}
				else{
					$media->media_type_id=2;
				}
								
				$media->save();
					
					// $post->media()->attach($request->$image);		
				}

			 return redirect()->route('profile');
	

	}
    public function editPost($id){
    	$post= post::findOrFail($id);
		$users = Auth::user();
		$notifications_request= [];
		foreach ( $users->unreadNotifications as $not) {
			if($not->data['type'] == 'send_request'){
				array_push($notifications_request,  $not);
			}
	
		}
		// // echo "<pre>";
		$notifications_news= [];
		foreach ( $users->unreadNotifications as $not) {
			if($not->data['type'] == 'confirmed'){
				array_push($notifications_news,  $not);
			}
		
		}

    	return view('editPost',compact('post','users','notifications_request','notifications_news'));

    }

            public function newsFeed()
    {
		$users = Auth::user();
    	$sports = sport::all(); 
    	$clubs = club::all();
    	$countries = geo_countries::all();
    	$userlikes =  $users->like()->where('user_id',Auth::id())->get();
    	$posts =  $users->posts()->orderBy('created_at','DESC')->get();
		$following = Auth::user()->following()->where('confirmed',1)->get();
		$followers = Auth::user()->followers()->where('confirmed',1)->get();
    	$last_post = $users->posts()->orderBy('created_at','DESC')->first();
    	$last_video = 0;
    	if(!is_null($last_post))
    	{
    		$last_video = $last_post->lastVideo();

    	}
    	$postsfollowing =  $following->posts()->orderBy('created_at','DESC')->get();
    	$postsfollowers =  $followers->posts()->orderBy('created_at','DESC')->get();
    	
		return view('newsfeed',compact('followers','following','users','sports',
		'clubs','countries','userlikes','postsuser'));
    }




}
