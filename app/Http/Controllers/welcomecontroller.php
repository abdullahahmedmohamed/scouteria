<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\type;
use App\gender;
use App\ geo_countries;
use App\sport;
use Hash;
use Auth;
use Illuminate\Support\Facades\Mail;
// use App\Mail\Scoutria;

class welcomecontroller extends Controller
{
	public function index()
	{
		$type = type::all();
		$genders = gender::all();
		$countries = geo_countries::all();
		$sports = sport::all();
		return view('welcomes', compact('type','genders','countries','sports'));
	}
	public function registration(request $request)
	{
		/*$this->validate(Request(),[
		 'email' => 'required|email|unique:users,email,',
	    'name' => "required",
	    'password' => "required|min:6"
		]);
		*/
		

		$users = new User ;
		$users->name = $request->name;
		$users->email = $request->email;
		$users->password = Hash::make($request->password);
		$users->type_id = $request->type_id;
		$users->country_id = $request->country_id;
		$users->gender_id = $request->gender_id;
		$users->sport_id = $request->sport_id;	
		if($request->gender_id ==1){
			$users->image = "m1.jpg";
		}
		if($request->gender_id==2){
			$users->image = "m2.jpg";
		}

		$users->save();
				

	

       // Mail::send('email.email',	['name'=> $users->name], function ($message) use ($users)
       // {

       //     $message->from('info@scouteria.com', 'Scoutria');

       //     $message->to($users->email);

       // });
		return redirect()->route('doneregestration');

	}
	public function listuser(){
		$users = User::orderBy('id','DESC')->get();
		return view('admin.users.list',compact('users')) ;


	}


	public function delete_user($id)
	{
		$users = User::findOrFail($id)->delete();
		return redirect()->route('listuser');


	}
	public function ajax(request $request){

		$email= $request->email;
		$user = User::where('email',$email)->first();
		if(!is_null($user))
		{
					return response()->json(['response'=>0]);

		}else{
					return response()->json(['response'=>1]);

		}

	} 
	
	public function home()
	{
		//return view('home');
	}

	public function email()
	{
		return view('email.email');
	}
	public function doneregestration()
	{
		return view('doneregestration');
	}
}

