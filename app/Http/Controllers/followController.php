<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\type;
use App\gender;
use App\ geo_countries;
use App\sport;
use App\club;
use App\formerclub;
use App\FollowUser;
use App\likes;
use App\comment;
use App\Notifications\FollowNotification;
use Notification;
use Hash;
use Illuminate\Support\Facades\Mail;
 class followController extends Controller
{
	public function index()
	{
    	$users = Auth::user();
		$user = User::all();    	
		return view('showfreinds', compact('user'));
	}


	public function follow_user(Request $request)
	{
		//This will attach the request user with the logged in user

		Auth::user()->following()->attach([$request->user_id]);//for Example

		$follower = Auth::user();
		$user = User::find($request->user_id);
		$name = Auth::user()->name;
		$img = Auth::user()->image;

		$message = Auth::user()->name . ' followed you ';
		$array = [
			'message'=>$message ,
			'redirect_link'=>'profileuser' ,
			'type'=>'send_request' ,
			'user'=> Auth::id() ,
			'name'=>$name ,
			'image'=>$img

		];

		// $done= FollowUser::where('user_id', $user)->where('followed_user_id' ,$follower)->first();
		// if($done){


		// }
			Notification::send($user , new FollowNotification($array));
		return redirect()->route('profile');

	}


	public function unFollow(Request $request)
	{
		//This will attach the request user with the logged in user
		$follower = Auth::id();

		$user = $request->user;

		$f = FollowUser::where('user_id',$follower)->where("followed_user_id",$user)->first();
		$ff = FollowUser::where("user_id",$user)->where('followed_user_id',$follower)->first();
		if( $f ){
			$f->delete();

		}
		if( $ff){
			$ff->delete();
		}
			return redirect()->route('profile');

	}

	public function ConfirmFollow(Request $request)
	{
		$logged_in_user_id = Auth::id();
		$target_user_id =  $request->user_id;
		$notification_id =  $request->id;
		$data =FollowUser::where('user_id',$target_user_id)->where('followed_user_id',
		$logged_in_user_id)->first();
		$message = Auth::user()->name . ' Confirmed to follow you';
		$name = Auth::user()->name;
		$img = Auth::user()->image;
		$array = [
			'message' => $message ,
			'user' => $logged_in_user_id,
			'type'=>'confirmed' ,
			'name' => $name,
			'image' => $img,

		];

		if($data)
		{
    	$users = User::findOrFail($logged_in_user_id);
	//	 $users->unreadNotifications($notification_id)->update(['read_at' => now()]);
	 $users->unreadNotifications()->where('id',$notification_id)->update(['read_at' => now()]);
		$data->confirmed = 1;
		$data->save();

		//Save the Invers
		$save_inverse = new FollowUser;
		$save_inverse->user_id = $logged_in_user_id;
		$save_inverse->followed_user_id = $target_user_id;
		$save_inverse->confirmed = 1;
		$save_inverse->save();
		$target_user = User::find($target_user_id);
		Notification::send($target_user , new FollowNotification($array));

		return redirect('profile');
		}	

	}

	// 	public function readNotification(Request $request)
	// {
 //    	$notification_id =  $request->id; 
 //    	$users = Auth::id();

	//     $users->unreadNotifications()->where('id',$notification_id)->update(['read_at' => now()]);
  	
		
	// }


	public function rejectFollow(Request $request)
	{
		$logged_in_user_id = Auth::id();
		$target_user_id =  $request->user_id;
		$notification_id =  $request->id;
		$data =FollowUser::where('user_id',$target_user_id)->where('followed_user_id',
		$logged_in_user_id)->first();
		$message = Auth::user()->name . ' rejected to follow you';
		$name = Auth::user()->name;
		$img = Auth::user()->image;
		$array = [
			'message' => $message ,
			'user' => $logged_in_user_id,
			'type'=>'rejected' ,
			'name' => $name,
			'image' => $img,
 		];


		if($data)
		{
		$users = User::findOrFail($logged_in_user_id);
		 $users->unreadNotifications()->where('id',$notification_id)->update(['read_at' => now()]);
		$data->confirmed = 2;
		$data->save();
		$target_user = User::find($target_user_id);
		Notification::send($target_user , new FollowNotification($array));
				}



						return redirect('profile');
	}

	// public function readNotification(){
	// 	$logged_in_user_id = Auth::id();
	// 	$target_user_id =  $request->user_id;
	// 	$notification_id =  $request->notification_id;
	// 	$data =FollowUser::where('user_id',$target_user_id)->where('followed_user_id',
	// 	$logged_in_user_id)->first();
	// 	if($data)
	// 	{
	// 	$users = User::findOrFail($logged_in_user_id);
	// 	 $users->unreadNotifications()->where('id',$notification_id)->update(['read_at' => now()]);
	// 	$data->confirmed = 1;
	// 	$data->save();

	// 	}

	// }

	public function listFollowers(){

    	$users = Auth::user();
    	$following = Auth::user()->following;
		$followers = Auth::user()->followers;
		
		return view('listfollowers', compact('users'));
	}
 
	public function MyFollowing()
	{

    	$users = Auth::user();
		$following = Auth::user()->following()->where('confirmed',1)->get();
		$followers = Auth::user()->followers()->where('confirmed',1)->get();
		return view('listfollowers',compact('followers','following','users'));

	}


	public function like(Request $request)
	{
		$likes = new likes;
		$user = Auth::id();
		$post_id =  $request->post_id;
		$like =  1 ;
		$name = $request->name;
		$image = $request->image;
		$post_user =  $request->post_user;	
		if($likes)
		{
		$likes->user_id = $user;
		$likes->post_id = $post_id ;
		$likes->like = $like;
		$likes->save();


		}
		$message = Auth::user()->name . ' liked on your post';

		$array = [
			'message' => $message ,
			'type' => 'confirmed' ,
			'user' => $user,
			'name' => $name,
			'image' => $image,
			'post_id' => $post_id

		];		

		$postuser = User::findOrFail($post_user);
		Notification::send($postuser , new FollowNotification($array));	
		return redirect('profile');

	}


	public function unLike(Request $request)
	{
		//This will attach the request user with the logged in user
		$user = Auth::id();
		$post_id =  $request->post_id;

		$like = likes::where('post_id',$post_id)->where("user_id",$user)->first();
		if( $like ){
			$like->delete();
		}
	return redirect()->route('profile');

	}

	public function writeComment(Request $request)
	{
		$comments = new comment;
		$user = Auth::id();
		$name = $request->name;
		$image = $request->image;
		$post =  $request->post_id;
		$post_user =  $request->post_user;		
		$comment =  $request->comment;

		if($comments)
		{
		$comments ->user_id = $user;
		$comments ->post_id = $post ;
		$comments ->text = $comment;
		$message = Auth::user()->name . ' commented on your post';
		$comments ->save();


		}
  		$array = [
			'message' => $message ,
			'type' => 'confirmed' ,
			'user' => $user,
			'name' => $name,
			'image' => $image,
			'post_id' => $post

		];
		$postuser = User::findOrFail($post_user);
		Notification::send($postuser , new FollowNotification($array));	

		return redirect('profile');

	}

	public function editComment(Request $request ,$id)
	{
		$comment = comment::find($id);

		if($comment)
		{

		$comment->text =  $request->comment ;
		$comment->save();


		}	
		return redirect('profile');

	}

	public function removeComment(Request $request ,$id)
	{
		$comment = comment::find($id);

		if($comment)
		{
		$comment->delete();


		}	
		return redirect('profile');

	}
	/*	public function listNotifications()
	{
    	$users = Auth::user();
		    	
		return view('listNotifications', compact('users'));
	}*/
}
