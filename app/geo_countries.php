<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class geo_countries extends Model
{
    protected $table = 'geo_countries';

        public function geo_countries()
        {
            return $this->hasOne('App\users','country_id');
        }
}
